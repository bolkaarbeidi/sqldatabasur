-- Dropping tables ----------------------------------------------------------------------
drop table employee;
drop table child;
drop table partner;
drop table registration;
drop table transaction;
drop table bookedTransaction;
drop table account;
drop table accounttype;
drop table client;
drop table zip;
DROP TABLE rateCalculation;
DROP TABLE bookingtransactions_data;

-- Creating tables ----------------------------------------------------------------------
create table zip (
  zipcode NUMBER(20) not null,
  city VARCHAR2(60) not null,
  primary key (zipcode)
);


create table client (
  firstname VARCHAR2(20) not null,
  middlename VARCHAR2(255),
  lastname VARCHAR2(40) not null,
  clientid NUMBER(10),
  zipcode NUMBER(20) not null,
  dob DATE not null,
  ptal VARCHAR(10) not null,
  primary key(clientid),
  foreign key (zipcode) references zip (zipcode)
);

create table child (
  childid NUMBER(10) NOT NULL,
  parentid NUMBER(10) NOT NULL,
  primary key (childid, parentid),
  foreign key (childid) references client (clientid),
  foreign key (parentid) references client (clientid)
);

create table partner (
  partner1 NUMBER(10) not null,
  partner2 NUMBER(10) not null,
  primary key (partner1, partner2),
  foreign key (partner1) references client (clientid),
  foreign key (partner2) references client (clientid)
);


create table employee (
  role VARCHAR2(30) not null,
  salary NUMBER(20) not null,
  employeeid NUMBER(10),
  clientid NUMBER(10) not null,
  primary key(employeeid),
  foreign key (clientid) references client (clientid)
);


create table registration (
  name VARCHAR2(20) not null,
  bankid NUMBER(20),
  balance NUMBER(36,2) not null,
  equity NUMBER(36,2) not null,
  primary key (bankid),
  check ((balance + equity) = 0)
  --check (balance > 0),
  --check (equity > 0)
);


create table accounttype (
  type NUMBER(2) not null,
  credit NUMBER(36) not null,
  debit NUMBER(36) not null,
  debitrates NUMBER(20) not null,
  creditrates NUMBER(20) not null,
  overdrawrates NUMBER(20) not null,
  primary key (type),
  CHECK (type BETWEEN 10 AND 99)
);


create table account (
  clientid NUMBER(10) not null,
  balance NUMBER(36,2) NOT NULL,
  maxoverdraw NUMBER(20,2) NOT NULL,
  type NUMBER(2) NOT NULL,
  account_number NUMBER(11),
  name VARCHAR2(20) not null,
  primary key (account_number),
  foreign key (type) references accounttype (type),
  foreign key (clientid) references client (clientid)
);


create table transaction (
  -- Fields
  transactionid NUMBER(36) NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  transactiontype NUMBER(5) NOT NULL,
  account_number NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20,2) NOT NULL,
  cid NUMBER NOT NULL,
  --account_number_out NUMBER(20) NOT NULL,
  --account_number_in NUMBER(20) NOT NULL,

  -- Constraints
  primary key(transactionid),
  foreign key (account_number) references account (account_number),
  foreign key (cid) references client (clientid)
  --foreign key (account_number_out) references account (account_number),
  --foreign key (account_number_in) references account (account_number)
);


CREATE TABLE bookedTransaction (
  -- Fields
  transactionid NUMBER(36) NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  transactiontype NUMBER(5) NOT NULL,
  account_number NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20,2) NOT NULL,
  cid NUMBER NOT NULL,
  --account_number_out NUMBER(20) NOT NULL,
  --account_number_in NUMBER(20) NOT NULL,

  -- Constraints
  primary key(transactionid),
  foreign key (account_number) references account (account_number),
  foreign key (cid) references client (clientid)
  --foreign key (account_number_out) references account (account_number),
  --foreign key (account_number_in) references account (account_number)
);

CREATE TABLE rateCalculation (	
  fromDate DATE NOT NULL, 
  toDate DATE NOT NULL, 
  sumRate NUMBER(12,2), 
  ratePercent NUMBER(2,0), 
  PRIMARY KEY (fromDate)
);

CREATE TABLE bookingtransactions_data (
  id NUMBER,
  last_booking TIMESTAMP
);

-- Zip codes ----------------------------------------------------------------------
insert into Zip values (927, 'Akrar');
insert into Zip values (160, 'Argir');
insert into Zip values (165, 'Argir (postsmoga)');
insert into Zip values (726, 'Ánir');
insert into Zip values (727, 'Árnafjørður');
insert into Zip values (386, 'Bøur');
insert into Zip values (235, 'Dalur');
insert into Zip values (735, 'Depil');
insert into Zip values (470, 'Eiði');
insert into Zip values (478, 'Elduvík');
insert into Zip values (870, 'Fámjin');
insert into Zip values (825, 'Froðba');
insert into Zip values (530, 'Fuglafjørður');
insert into Zip values (477, 'Funningsfjørður');
insert into Zip values (475, 'Funningur');
insert into Zip values (387, 'Gásadalur');
insert into Zip values (476, 'Gjógv');
insert into Zip values (625, 'Glyvrar');
insert into Zip values (510, 'Gøta');
insert into Zip values (666, 'Gøtueiði');
insert into Zip values (511, 'Gøtugjógv');
insert into Zip values (440, 'Haldarsvík');
insert into Zip values (785, 'Haraldssund');
insert into Zip values (767, 'Hattarvík');
insert into Zip values (695, 'Hellur');
insert into Zip values (280, 'Hestur');
insert into Zip values (420, 'Hósvík');
insert into Zip values (960, 'Hov');
insert into Zip values (188, 'Hoyvík');
insert into Zip values (796, 'Húsar');
insert into Zip values (230, 'Húsavík');
insert into Zip values (850, 'Hvalba');
insert into Zip values (430, 'Hvalvík');
insert into Zip values (740, 'Hvannasund');
insert into Zip values (187, 'Hvítanes');
insert into Zip values (494, 'Innan Glyvur');
insert into Zip values (180, 'Kaldbak');
insert into Zip values (185, 'Kaldbaksbotnur');
insert into Zip values (766, 'Kirkja');
insert into Zip values (175, 'Kirkjubøur');
insert into Zip values (700, 'Klaksvík');
insert into Zip values (495, 'Kolbanargjógv');
insert into Zip values (410, 'Kollafjørður');
insert into Zip values (285, 'Koltur');
insert into Zip values (780, 'Kunoy');
insert into Zip values (340, 'Kvívík');
insert into Zip values (626, 'Lambareiði');
insert into Zip values (627, 'Lambi');
insert into Zip values (438, 'Langasandur');
insert into Zip values (520, 'Leirvík');
insert into Zip values (335, 'Leynar');
insert into Zip values (466, 'Ljósá');
insert into Zip values (926, 'Lopra');
insert into Zip values (370, 'Miðvágur');
insert into Zip values (797, 'Mikladalur');
insert into Zip values (496, 'Morskranes');
insert into Zip values (737, 'Múli');
insert into Zip values (388, 'Mykines');
insert into Zip values (655, 'Nes (Eysturoy)');
insert into Zip values (925, 'Nes (Vágur)');
insert into Zip values (437, 'Nesvík');
insert into Zip values (730, 'Norðdepil');
insert into Zip values (725, 'Norðoyri');
insert into Zip values (178, 'Norðradalur');
insert into Zip values (512, 'Norðragøta');
insert into Zip values (460, 'Norðskáli');
insert into Zip values (736, 'Norðtoftir');
insert into Zip values (270, 'Nólsoy');
insert into Zip values (690, 'Oyndarfjørður');
insert into Zip values (400, 'Oyrarbakki');
insert into Zip values (415, 'Oyrareingir');
insert into Zip values (450, 'Oyri');
insert into Zip values (950, 'Porkeri');
insert into Zip values (640, 'Rituvík');
insert into Zip values (620, 'Runavík');
insert into Zip values (436, 'Saksun');
insert into Zip values (600, 'Saltangará');
insert into Zip values (656, 'Saltnes');
insert into Zip values (360, 'Sandavágur');
insert into Zip values (210, 'Sandur');
insert into Zip values (860, 'Sandvík');
insert into Zip values (497, 'Selatrað');
insert into Zip values (416, 'Signabøur');
insert into Zip values (236, 'Skarvanes');
insert into Zip values (485, 'Skálabotnur');
insert into Zip values (220, 'Skálavík');
insert into Zip values (480, 'Skáli');
insert into Zip values (665, 'Skipanes');
insert into Zip values (240, 'Skopun');
insert into Zip values (260, 'Skúvoy');
insert into Zip values (336, 'Skælingur');
insert into Zip values (286, 'Stóra Dímun');
insert into Zip values (490, 'Strendur');
insert into Zip values (435, 'Streymnes');
insert into Zip values (330, 'Stykkið');
insert into Zip values (970, 'Sumba');
insert into Zip values (186, 'Sund');
insert into Zip values (465, 'Svínáir');
insert into Zip values (765, 'Svínoy');
insert into Zip values (795, 'Syðradalur (Kal.)');
insert into Zip values (177, 'Syðradalur (Str.)');
insert into Zip values (513, 'Syðrugøta');
insert into Zip values (660, 'Søldarfjørður');
insert into Zip values (380, 'Sørvágur');
insert into Zip values (445, 'Tjørnuvík');
insert into Zip values (650, 'Toftir');
insert into Zip values (100, 'Tórshavn');
insert into Zip values (110, 'Tórshavn (postsmoga)');
insert into Zip values (826, 'Trongisvágur');
insert into Zip values (798, 'Trøllanes');
insert into Zip values (800, 'Tvøroyri');
insert into Zip values (900, 'Vágur');
insert into Zip values (358, 'Válur');
insert into Zip values (385, 'Vatnsoyrar');
insert into Zip values (176, 'Velbastaður');
insert into Zip values (350, 'Vestmanna');
insert into Zip values (750, 'Viðareiði');
insert into Zip values (928, 'Víkarbyrgi');
insert into Zip values (645, 'Æðuvík');
insert into Zip values (827, 'Øravík');

-- Dropping sequences ----------------------------------------------------------------------
DROP SEQUENCE account_number;
DROP SEQUENCE client_id_seq;
DROP SEQUENCE transaction_id_seq;
DROP SEQUENCE transaction_pair_id_seq;
DROP SEQUENCE BOOK_TRANSACTIONS_SEQUENCE;
DROP SEQUENCE employee_id_seq;

-- Creating sequences ----------------------------------------------------------------------
CREATE SEQUENCE account_number MINVALUE 1 MAXVALUE 99999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE client_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE transaction_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 873 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE transaction_pair_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 317 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE BOOK_TRANSACTIONS_SEQUENCE;
CREATE SEQUENCE employee_id_seq;

-- Creating procedures ----------------------------------------------------------------------
-- Transaction procedure
create or replace PROCEDURE createTransaction 
(
  -- Input parameters
  account_number_out IN NUMBER,
  account_number_in IN NUMBER,
  message in VARCHAR2,
  amount in NUMBER,
  cid in NUMBER
) IS
new_balance NUMBER;
pairid NUMBER;
current_balance NUMBER;
limit NUMBER;
account_type NUMBER;
BEGIN
  DBMS_OUTPUT.put_line('test');
  IF (amount >= 0) THEN
    
    SELECT balance INTO current_balance FROM account WHERE account_number = account_number_out;
    current_balance := current_balance - amount;
    SELECT type INTO account_type FROM account WHERE account_number = account_number_out;
    IF (account_type = 15) THEN
      limit := 0;
    ELSE
      SELECT maxoverdraw INTO limit FROM account WHERE account_number = account_number_out;
      
    END IF;
    
    IF (current_balance >= limit) THEN
      DBMS_OUTPUT.put_line('inner test');
      --INSERT INTO transaction VALUES (1002, account_number_out, account_number_in, TRANSACTION_ID_SEQ.nextval, message, amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.nextval);
      --INSERT INTO transaction VALUES (1002, account_number_in, account_number_out, TRANSACTION_ID_SEQ.nextval, message, -amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.currval);
      SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO pairid FROM dual;
      INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, pairid, 1002, account_number_out, SYSDATE, SYSDATE, message, -amount, cid);
      INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, pairid, 1002, account_number_in, SYSDATE, SYSDATE, message, amount, cid);
      
      --SELECT balance INTO new_balance FROM account WHERE account_number = account_number_out;
      --UPDATE account
      --SET balance = balance - amount
      --WHERE account_number = account_number_out;
      --SELECT balance INTO new_balance FROM account WHERE account_number = account_number_in;
      --UPDATE account
      --SET balance = balance + amount
      --WHERE account_number = account_number_in;
    END IF;
  END IF;
END createTransaction;
/

-- Create account procedure
create or replace PROCEDURE CREATEACCOUNT 
(
  overdraw IN NUMBER
, clientid IN NUMBER
, accounttype IN NUMBER
, name IN VARCHAR2
) IS
next_account NUMBER;
tvorsum number;
rest number;
acc varchar2(11);
accnumber NUMBER;
acctype VARCHAR2(6);
bankreg VARCHAR2(4);
BEGIN
loop 
      select account_number.nextval into next_account from dual;
      -- skrásetingarnummar er 6460 og kontonummar byrjar við 41 fyri vanligar konti
      -- síðan kemur eitt raðtal frá teljaranum
      acctype := concat('6460', to_char(accounttype));
      acc := concat(acctype, lpad(to_char(next_account), 4, '0')) ;
      tvorsum := 5 * to_number(substr(acc, 1, 1)) +
                 4 * to_number(substr(acc, 2, 1)) +
                 3 * to_number(substr(acc, 3, 1)) +
                 2 * to_number(substr(acc, 4, 1)) +
                 7 * to_number(substr(acc, 5, 1)) +
                 6 * to_number(substr(acc, 6, 1)) +
                 5 * to_number(substr(acc, 7, 1)) +
                 4 * to_number(substr(acc, 8, 1)) +
                 3 * to_number(substr(acc, 9, 1)) +
                 2 * to_number(substr(acc, 10, 1));
      rest := 11 - mod(tvorsum,11);
      exit when rest < 10;  -- endurtak um rest er 10
    end loop;
   accnumber := to_number(acc) * 10 + rest;
  INSERT INTO account VALUES (clientid, 0, -overdraw, accounttype, accnumber, name);
  --INSERT INTO clientaccount VALUES (ACCOUNT_ID_SEQUENCE.currval, clientid);
END CREATEACCOUNT;
/

-- Create bank procedure
create or replace PROCEDURE CREATEBANK 
(
  NAME IN VARCHAR2 
, START_MONEY IN NUMBER
--, ACCOUNT_BALANCE IN NUMBER 
--, ACCOUNT_EQUITY IN NUMBER
, BANKID IN NUMBER
) AS 
BEGIN
  --INSERT INTO registration VALUES (name, bankid, -start_money, start_money, account_balance, account_equity);
  INSERT INTO registration VALUES (name, bankid, -start_money, start_money);
END CREATEBANK;
/

create or replace PROCEDURE CREATECHILD
(
 childid IN number, 
 parentid IN number
 ) AS
 BEGIN
  INSERT INTO child values (childid, parentid);
END Createchild;
/

create or replace PROCEDURE CREATECLIENT
(
  FIRSTNAME IN VARCHAR2 
, MIDDLENAME IN VARCHAR2 
, LASTNAME IN VARCHAR2 
, ZIPCODE IN NUMBER 
, DOB IN DATE
, PTAL IN VARCHAR2
) AS 
BEGIN
  INSERT INTO client VALUES (FIRSTNAME, MIDDLENAME, LASTNAME, CLIENT_ID_SEQ.nextval, ZIPCODE, DOB, PTAL);
END CREATECLIENT;
/

-- Create partner procedure
create or replace PROCEDURE CREATEPARTNER
(
 Partner1 IN number, 
 partner2 IN number
 ) AS
 BEGIN
  INSERT INTO partner values (partner1, partner2);
  INSERT INTO partner values (partner2, partner1);
END CREATEPARTNER;
/

create or replace PROCEDURE DEPOSIT 
( 
  AMOUNT IN NUMBER
, ACCOUNTNR IN NUMBER
, MESSAGE IN VARCHAR2
, cid IN NUMBER
) IS
equity2 NUMBER;
balance2 NUMBER;
account_equity NUMBER;
account_balance NUMBER;
transaction_pair NUMBER;
bankclient NUMBER;
BEGIN
  SELECT bankid INTO bankclient FROM registration WHERE rownum = 1;
  SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO transaction_pair FROM dual;
  SELECT equity INTO equity2 FROM registration WHERE bankid = bankclient;
  SELECT balance INTO balance2 FROM registration WHERE bankid = bankclient;
  
  SELECT account_number INTO account_equity FROM account WHERE clientid = bankclient and type=89;
  SELECT account_number INTO account_balance FROM account WHERE clientid = bankclient and type=88;
  
  equity2 := equity2 - amount;
  balance2 := balance2 + amount;
  UPDATE registration SET equity = equity2, balance = balance2 WHERE bankid = 1;
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, accountnr, SYSDATE, SYSDATE, message, amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_equity, SYSDATE, SYSDATE, message, -amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_balance, SYSDATE, SYSDATE, message, amount, cid);
END DEPOSIT;
/

-- Withdraw procedure
create or replace PROCEDURE WITHDRAW  
( 
  AMOUNT IN NUMBER
, ACCOUNTNR IN NUMBER
, MESSAGE IN VARCHAR2
, cid IN NUMBER
) IS
equity2 NUMBER;
balance2 NUMBER;
account_equity NUMBER;
account_balance NUMBER;
transaction_pair NUMBER;
bankclient NUMBER;
BEGIN
  SELECT bankid INTO bankclient FROM registration WHERE rownum = 1;
  SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO transaction_pair FROM dual;
  SELECT equity INTO equity2 FROM registration WHERE bankid = bankclient;
  SELECT balance INTO balance2 FROM registration WHERE bankid = bankclient;
  
  SELECT account_number INTO account_equity FROM account WHERE clientid = bankclient and type=89;
  SELECT account_number INTO account_balance FROM account WHERE clientid = bankclient and type=88;
  
  equity2 := equity2 + amount;
  balance2 := balance2 - amount;
  UPDATE registration SET equity = equity2, balance = balance2 WHERE bankid = 1;
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, accountnr, SYSDATE, SYSDATE, message, -amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_equity, SYSDATE, SYSDATE, message, amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_balance, SYSDATE, SYSDATE, message, -amount, cid); 
END WITHDRAW;
/

create or replace PROCEDURE INITIALIZEBANK(
  fname VARCHAR2
, mname VARCHAR2
, lname VARCHAR2
, postnumber NUMBER
, dob DATE
, bankname VARCHAR2
, startmoney NUMBER
)
IS
bankclient NUMBER;
equity_account NUMBER;
balance_account NUMBER;
BEGIN
  -- Ger konto typur
  -- Equity (Bank)
  INSERT INTO accounttype VALUES (89, 0, 0, 0, 0, 0);
  -- Balance (Bank)
  INSERT INTO accounttype VALUES (88, 0, 0, 0, 0, 0);
  -- Credit
  INSERT INTO accounttype VALUES (14, 0, 0, 0, 0, 0);
  -- Debit
  INSERT INTO accounttype VALUES (15, 0, 0, 0, 0, 0);
  -- Other
  INSERT INTO accounttype VALUES (16, 0, 0, 0, 0, 0);
  
  CREATECLIENT(fname, mname, lname, postnumber, dob, '111111-111');
  SELECT clientid INTO bankclient FROM client WHERE firstname = fname;
  CREATEEMPLOYEE('Boss', 1000, bankclient);  
  equity_account := CREATEACCOUNT2(startmoney, bankclient, 88, 'EQUITY');
  balance_account := CREATEACCOUNT2(startmoney, bankclient, 89, 'BALANCE');
  CREATEBANK(bankname, startmoney, bankclient);
  UPDATE account
    SET balance = -startmoney
    WHERE account_number = equity_account;
  
  UPDATE account
    SET balance = startmoney
    WHERE account_number = balance_account;
  
END INITIALIZEBANK;
/

create or replace PROCEDURE CREATECLIENTS IS 
tempclientid NUMBER;
BEGIN
  CREATECLIENT('Rúni', 'Klein', 'Hansen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Rúni';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Torkil', 'Janusarson', 'Thomsen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Torkil';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Jákup', 'Pæturssonur', 'Dam', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Jákup';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Heidi', 'Jónsveinsdóttir', 'Simonsen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Heidi';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
END CREATECLIENTS;
/

create or replace PROCEDURE BOOKKEEPING (
  startdate DATE,
  enddate DATE
)
AS
CURSOR c_transactions is
  select transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, cid FROM transaction;
b_transactionid NUMBER;
b_transactionpair NUMBER;
b_transactiontype NUMBER;
b_account_number NUMBER;
b_transactiondate DATE;
b_valuedate DATE;
b_message VARCHAR2(255);
b_amount NUMBER;
b_cid NUMBER;
BEGIN
  SAVEPOINT before_booking; 
  DBMS_OUTPUT.put_line('Beginning to book transactions');
  OPEN c_transactions;
  LOOP
    FETCH c_transactions INTO b_transactionid, b_transactionpair, b_transactiontype, b_account_number, b_transactiondate, b_valuedate, b_message, b_amount, b_cid;
    EXIT WHEN c_transactions%notfound;
    IF startdate <= b_transactiondate and b_transactiondate < enddate THEN
      DBMS_OUTPUT.put_line(b_transactionid);
      INSERT INTO bookedtransaction -- (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount)
      VALUES (b_transactionid, b_transactionpair, b_transactiontype, b_account_number, b_transactiondate, systimestamp, b_message, b_amount, b_cid);
      DELETE FROM transaction WHERE transactionid = transactionid;
    END IF;
  END LOOP;
  EXCEPTION
  --DBMS_OUTPUT.put_line('Exception occured rolling back. [Bookkeeping]');
  WHEN OTHERS THEN
  ROLLBACK TO before_booking;
  COMMIT;
END BOOKKEEPING;
/

CREATE OR REPLACE PROCEDURE BOOKKEEPING2
AS
current_time TIMESTAMP;
last_time TIMESTAMP;
BEGIN
  SELECT systimestamp INTO current_time FROM dual;
  SELECT max(last_booking) INTO last_time FROM bookingtransactions_data;
  bookkeeping(last_time, current_time);
  INSERT INTO BOOKINGTRANSACTIONS_DATA VALUES (BOOK_TRANSACTIONS_SEQUENCE.nextval, current_time);
END BOOKKEEPING2;
/

create or replace PROCEDURE CREATEEMPLOYEE
(
  ROLE IN VARCHAR2 
, SALARY IN NUMBER 
, CLIENTID IN NUMBER
) AS 
BEGIN
  INSERT INTO employee VALUES (ROLE, SALARY, EMPLOYEE_ID_SEQ.nextval, CLIENTID);
END CREATEEMPLOYEE;
/

create or replace procedure CALCULATERATE AS 
current_time DATE;
BEGIN
    SELECT sysdate INTO current_time FROM dual;
    insert into ratecalculation VALUES (null, current_time, 99, 15);
END CALCULATERATE;
/

-- Create functions ----------------------------------------------------------------------
create or replace FUNCTION calcRate(
    fromDate_p  DATE,
    toDate_p  DATE,
    rateFactor NUMBER)
  RETURN NUMBER
AS
  -- Fyrst nakrir temp variablar til lokala utrokning
  sum_account NUMBER(12,2);
  sum_total   NUMBER(12,2);
  temp_saldo  NUMBER(12,2);
  temp_date   DATE;
  bank_account_equity NUMBER;
  bank_account_balance NUMBER;
  -- So ein cursara til at renna ígjøgnum kontonummur
  CURSOR accountCursor IS
  SELECT a.* 
  FROM account a 
  ORDER BY a.account_number; -- riggar
  -- Og ein cursara til at renna ígjøgnum bókingar á hvørjari konto
  CURSOR transCursor(c_from DATE, c_to DATE , c_account VARCHAR2) IS
    SELECT t.*
    FROM bookedtransaction t
    WHERE t.transactionDate > c_from
    AND t.transactionDate  <= c_to
    AND t.account_number    = c_account
    ORDER BY t.transactionDate;

BEGIN
  SELECT account_number INTO bank_account_equity FROM account WHERE clientid = 1 AND type = 89;
  SELECT account_number INTO bank_account_balance FROM account WHERE clientid = 1 AND type = 88;
  --dbms_output.put('sum total ');
  --set serveroutput on
  --begin   dbms_output.put_line('dbms_output.put_line(''soem text...'');');end;
  -- nullstilla sum
  sum_total :=0;
  -- renn ígjøgnum allar konti
  FOR accountRecords IN accountCursor LOOP
    IF accountRecords.account_number = bank_account_equity OR accountRecords.account_number = bank_account_balance THEN
      CONTINUE;
    END IF;
    -- nullstilla sum og frá dato, so hetta byrjar umaftur fyri hvørja konto
    sum_account := 0;
    temp_date   := fromDate_p;
    -- finn byrjunarsaldo fyri tíðarskeið/konto
    SELECT NVL(SUM(t.amount),0) INTO temp_saldo
    FROM transaction t
    WHERE t.account_number  = accountRecords.account_number and
          t.transactionDate <= fromDate_p;
    -- renn ígjøgnum bókingar
    FOR transRecords IN transCursor(fromDate_p, toDate_p, accountRecords.account_number) LOOP
      -- rokna rentu
      sum_account := sum_account + (trunc(transRecords.transactionDate) - temp_date) * temp_saldo * rateFactor/360;
      -- dagfør saldo og dagfesting til næsta umfar
      temp_saldo  := temp_saldo + transRecords.amount;
      temp_date   := trunc(transRecords.transactionDate);
    END LOOP;
    -- rokna rentu um har eru dagar eftir í mánaðinum
    IF temp_date   < toDate_p THEN
      sum_account := sum_account + (toDate_p - temp_date) * temp_saldo * rateFactor/360;
    END IF;
    -- bóka rentu á konto
    dbms_output.put('Rate of ' || accountRecords.account_number || ': ' || sum_account);
    IF sum_account != 0 THEN
      INSERT INTO transaction
        --( transactiondate,  account_number, amount)
        (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, cid)
        VALUES
        --(toDate_p, accountRecords.account_number, sum_account);
        (TRANSACTION_ID_SEQ.nextval, TRANSACTION_PAIR_ID_SEQ.nextval, 1010, accountRecords.account_number, sysdate, sysdate, 'Renta', sum_account, 1);
    END IF;
    --dagfør total
    sum_total := sum_total + sum_account;
  END LOOP;
  -- returnera total til kallara
  RETURN sum_total;
  dbms_output.put('sum total ' || sum_total);
END;
/

create or replace FUNCTION CREATEACCOUNT2  
(
  overdraw IN NUMBER
, clientid IN NUMBER
, accounttype IN NUMBER
, name IN VARCHAR2
) RETURN NUMBER IS
next_account NUMBER;
tvorsum number;
rest number;
acc varchar2(11);
accnumber NUMBER;
acctype VARCHAR2(6);
bankreg VARCHAR2(4);
BEGIN
loop 
      select account_number.nextval into next_account from dual;
      -- skrásetingarnummar er 6460 og kontonummar byrjar við 41 fyri vanligar konti
      -- síðan kemur eitt raðtal frá teljaranum
      acctype := concat('6460', to_char(accounttype));
      acc := concat(acctype, lpad(to_char(next_account), 4, '0')) ;
      tvorsum := 5 * to_number(substr(acc, 1, 1)) +
                 4 * to_number(substr(acc, 2, 1)) +
                 3 * to_number(substr(acc, 3, 1)) +
                 2 * to_number(substr(acc, 4, 1)) +
                 7 * to_number(substr(acc, 5, 1)) +
                 6 * to_number(substr(acc, 6, 1)) +
                 5 * to_number(substr(acc, 7, 1)) +
                 4 * to_number(substr(acc, 8, 1)) +
                 3 * to_number(substr(acc, 9, 1)) +
                 2 * to_number(substr(acc, 10, 1));
      rest := 11 - mod(tvorsum,11);
      exit when rest < 10;  -- endurtak um rest er 10
    end loop;
   accnumber := to_number(acc) * 10 + rest;
  INSERT INTO account VALUES (clientid, 0, -overdraw, accounttype, accnumber, name);
  --INSERT INTO clientaccount VALUES (ACCOUNT_ID_SEQUENCE.currval, clientid);
  return accnumber;
END CREATEACCOUNT2;
/

create or replace FUNCTION ISEMPLOYEE (
  clientid NUMBER
)
RETURN BOOLEAN AS
  isEmployeeSize NUMBER;
  isEmployee BOOLEAN;
BEGIN
  SELECT COUNT(CLIENTID) INTO isEmployeeSize FROM EMPLOYEE;
  IF isEmployeeSize > 0 THEN
    isEmployee := true;
  END IF;
  RETURN isEmployee;
END ISEMPLOYEE;
/

-- Create views ----------------------------------------------------------------------
CREATE OR REPLACE VIEW accounts_summary (transaction, account) AS
SELECT A.account_number, SUM(T.amount) FROM transaction T
INNER JOIN account A ON T.account_number = A.account_number GROUP BY A.account_number;

--CREATE OR REPLACE VIEW transactions_view (message, account_number, amount, account_number_from)
--AS SELECT message, account_number_in, amount, account_number_out
--FROM transaction;

CREATE OR REPLACE VIEW CLIENTRELATION (CLIENTID, FIRSTNAME, LASTNAME, PARTNERID) AS 
select c.clientid, c.firstname, c.lastname, p.partner2 as partnerID
from client c, partner p
where c.CLIENTID = p.PARTNER1
  UNION
select c.clientid, c.firstname, c.lastname, p.partner1
from client c, partner p
where c.CLIENTID = p.PARTNER2
  Union
select c.clientid, c.firstname, c.lastname, d.childid
from client c, CHILD d
where c.CLIENTID = d.PARENTID;

--CREATE OR REPLACE VIEW transaction_pairs (transactionid, transactionpair, transactiontype, account_number_in, transactiondate, valuedate, message, amount_in, account_number_out, amount_out) AS
--SELECT t1.*, t2.account_number, t2.amount FROM transaction t1 LEFT JOIN transaction t2 ON t1.transactionpair = t2.transactionpair AND t1.transactionid != t2.transactionid;

CREATE OR REPLACE VIEW transaction_summary (account_number, valuedate, message, amount, name) AS
SELECT T1.account_number, T1.valuedate, T1.message, T1.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name FROM transaction T1 LEFT JOIN account a ON T1.account_number = a.account_number LEFT JOIN client c ON a.clientid = c.clientid;

CREATE OR REPLACE VIEW partner_accounts AS
SELECT c.firstname, c.middlename, c.lastname, c.clientid, p.partner1 personlink, a.balance, a.account_number, a.maxoverdraw, a.type, a.name FROM client c LEFT JOIN partner p ON c.clientid = p.partner2 LEFT JOIN account a ON a.clientid = p.partner2;

CREATE OR REPLACE VIEW child_accounts AS
SELECT c.firstname, c.middlename, c.lastname, c.clientid, p.parentid personlink, a.balance, a.account_number, a.maxoverdraw, a.type, a.name FROM client c LEFT JOIN child p ON c.clientid = p.childid LEFT JOIN account a ON a.clientid = p.childid;



CREATE OR REPLACE VIEW ACCOUNT_TRANSACTIONS (ACCOUNT_NUMBER, TRANSACTIONDATE, MESSAGE, AMOUNT) AS 
SELECT account_number, valuedate, message, amount
FROM transaction;
 

--CREATE OR REPLACE VIEW TRANSACTION_PAIRS2 (TRANSACTIONID, TRANSACTIONPAIR, TRANSACTIONTYPE, ACCOUNT_NUMBER, TRANSACTIONDATE, VALUEDATE, MESSAGE, AMOUNT, ACCOUNT_NUMBER_1, AMOUNT_1) AS 
--SELECT t1.*, t2.account_number, t2.amount FROM transaction t1 LEFT JOIN transaction t2 ON t1.transactionpair = t2.transactionpair AND t1.transactionid != t2.transactionid;
 

CREATE OR REPLACE VIEW all_transactions_view (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, status) AS
SELECT bt.transactionid, bt.transactionpair, bt.transactiontype, bt.account_number, bt.transactiondate, bt.valuedate, bt.message, bt.amount, 'b' status FROM bookedtransaction bt
UNION
SELECT t.transactionid, t.transactionpair, t.transactiontype, t.account_number, t.transactiondate, t.valuedate, t.message, t.amount, 'P' status FROM transaction t;
 
CREATE OR REPLACE VIEW all_transactions_view_reduced (account_number, transactiondate, message, ammount, status) AS
SELECT account_number, valuedate, message, amount, status FROM ALL_TRANSACTIONS_VIEW;

CREATE OR REPLACE VIEW all_transactions (account_number, valuedate, message, amount, clientid, name, status) AS
SELECT bt.account_number, bt.valuedate, bt.message, bt.amount, a.clientid, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name, 'b' status FROM bookedtransaction bt inner join account a on a.account_number = bt.account_number inner join client c on c.clientid = a.clientid
union
SELECT t.account_number, t.valuedate, t.message, t.amount, a.clientid, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name, 'p' status FROM transaction t inner join account a on a.account_number = t.account_number inner join client c on c.clientid = a.clientid;


CREATE OR REPLACE VIEW transactions_all_uber (account_number, valuedate, message, amount, name, status, clientid, transactionpair) AS
SELECT distinct t2.account_number, t2.valuedate, t2.message, t1.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' as status, c.clientid, t1.transactionpair
FROM bookedtransaction t1
JOIN bookedtransaction t2
ON t1.transactionpair = t2.transactionpair
JOIN account a
ON t2.ACCOUNT_NUMBER = a.ACCOUNT_NUMBER
JOIN client c
ON a.clientid = c.clientid
WHERE t1.transactionpair = t1.transactionpair and t1.transactiontype != 1000 AND t1.account_number != t2.account_number
--WHERE t1.account_number = 64601400039 and t2.account_number != 64601400039 and t1.transactionpair = t1.transactionpair and t1.transactiontype != 1000
union
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' as status, c.clientid, t.transactionpair
FROM bookedtransaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid
WHERE t.transactiontype = 1000;


CREATE OR REPLACE VIEW transaction_summary2 (account_number, valuedate, message, amount, name, status) AS
SELECT * FROM (
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'p' status
FROM transaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid
UNION
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' status
FROM bookedtransaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid)
ORDER BY valuedate desc;


CREATE OR REPLACE VIEW PARTNER_ACCOUNTS2 (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT p.partner1, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name
FROM partner p
INNER JOIN account a
ON a.clientid = p.partner2;


CREATE OR REPLACE VIEW CHILD_ACCOUNTS2 (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT c.parentid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name
FROM child c
INNER JOIN account a
ON a.clientid = c.childid;


CREATE OR REPLACE VIEW CHILD_AND_PARTNER_ACCOUNTS (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT * FROM (
SELECT * FROM PARTNER_ACCOUNTS2
UNION
SELECT * FROM CHILD_ACCOUNTS2
);


CREATE OR REPLACE VIEW my_own_accounts (clientid, personlinkid, balance, maxoverdraw, type, account_number, account_name, name) AS
SELECT c.clientid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c.firstname || ' ' || c.middlename || ' ' || c.lastname name FROM account a
INNER JOIN client c
ON a.clientid = c.clientid;

CREATE OR REPLACE VIEW PARTNER_ACCOUNTS3 (clientid, personlinkid, balance, maxoverdraw, type, account_number, accountname, name)
AS
SELECT p.partner1, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name
FROM partner p
INNER JOIN account a
ON a.clientid = p.partner2
INNER JOIN client c
ON p.partner2 = c.clientid;

CREATE OR REPLACE VIEW CHILD_ACCOUNTS3 (clientid, personlinkid, balance, maxoverdraw, type, account_number, accountname, name)
AS
SELECT c.parentid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c1.firstname || ' ' || c1.middlename || ' ' || c1.lastname as name
FROM child c
INNER JOIN account a
ON a.clientid = c.childid
INNER JOIN client c1
ON c.childid = c1.clientid;

CREATE OR REPLACE VIEW my_own_accounts (clientid, personlinkid, balance, maxoverdraw, type, account_number, account_name, name) AS
SELECT c.clientid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c.firstname || ' ' || c.middlename || ' ' || c.lastname name FROM account a
INNER JOIN client c
ON a.clientid = c.clientid;

CREATE OR REPLACE VIEW COUPLE_TOTAL_BALANCE(CLIENTID, COUPLE_BALANCE) AS 
  select CLIENTID, COUPLE_BALANCE from 
   (Select clientid, sum(balance) as Couple_Balance
   from COUPLE_ACCOUNTS
   group by CLIENTID);

CREATE OR REPLACE VIEW FAMILY_ACCOUNTS ("CLIENTID", "PERSONLINKID", "BALANCE", "MAXOVERDRAW", "TYPE", "ACCOUNT_NUMBER", "ACCOUNTNAME", "NAME") AS 
  select "CLIENTID","PERSONLINKID","BALANCE","MAXOVERDRAW","TYPE","ACCOUNT_NUMBER","ACCOUNTNAME","NAME" from (
select * from partner_accounts3
union
select * from child_accounts3
union
select * from my_own_accounts);

-- Data that must be entered before triggers ----------------------------------------------------------------------
INSERT INTO rateCalculation VALUES ('01-DEC-2020', '31-DEC-2020', 99, 99);
INSERT INTO BOOKINGTRANSACTIONS_DATA VALUES (BOOK_TRANSACTIONS_SEQUENCE.nextval, TO_TIMESTAMP('2020-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS')); 

-- Create triggers ----------------------------------------------------------------------
-- rateCalculation (renturoknin)
CREATE OR REPLACE TRIGGER CALCRATE_INS before insert on rateCalculation
for each row
begin
  select max(toDate)+1 into :new.fromDate
  from rateCalculation;
  :new.sumrate := calcRate(:new.fromDate, :new.toDate, :new.ratepercent/100);
end;
/

-- Update balance on account when inserting into transaction table
CREATE OR REPLACE TRIGGER UPDATE_BALANCE 
BEFORE INSERT ON BOOKEDTRANSACTION 
FOR EACH ROW
DECLARE
new_account_number NUMBER;
new_balance NUMBER;
BEGIN
  new_account_number := :new.account_number;
  SELECT balance INTO new_balance FROM account WHERE account_number = new_account_number;
  new_balance := new_balance + :new.amount;
  UPDATE account
    SET balance = new_balance
    WHERE account_number = new_account_number;
END;
/

-- Ptal check, raises error if invalid ptal is given
CREATE OR REPLACE TRIGGER PTALCHECK 
BEFORE INSERT ON CLIENT
for each row
DECLARE
checkcipher NUMBER;
modcheck NUMBER;
ptalnew VARCHAR2(10);
BEGIN
  SELECT :new.ptal INTO ptalnew from dual;
  DBMS_OUTPUT.put_line('PTAL: ' || ptalnew);
  checkcipher :=   3 * to_number(substr(ptalnew, 1, 1)) +
                   2 * to_number(substr(ptalnew, 2, 1)) +
                   7 * to_number(substr(ptalnew, 3, 1)) +
                   6 * to_number(substr(ptalnew, 4, 1)) +
                   5 * to_number(substr(ptalnew, 5, 1)) +
                   4 * to_number(substr(ptalnew, 6, 1)) +
                   3 * to_number(substr(ptalnew, 8, 1)) +
                   2 * to_number(substr(ptalnew, 9, 1)) +
                   1 * to_number(substr(ptalnew, 10, 1));
  modcheck := mod(checkcipher,11);
  DBMS_OUTPUT.put_line('Modulus check gave: ' || modcheck);
  IF (modcheck != 0) THEN
    raise_application_error(-20000, 'Invalid P-Tal'); 
    --SELECT -1 INTO :new.pid FROM dual;
  END IF;
END;
/
-- Jobs ----------------------------------------------------------------------
-- Dropping jobs
BEGIN
  dbms_scheduler.drop_job(job_name => 'booking_transactions');
  dbms_scheduler.drop_job(job_name => 'calculating_rates');
END;
/

-- Rate calculations
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'calculating_rates', --name of job
   job_type           =>  'STORED_PROCEDURE', -- type
   job_action         =>  'calculaterate', -- procude name
   start_date         =>   SYSTIMESTAMP, -- byrjar frá tá ið hon verður gjørd
   --repeat_interval    =>  'FREQ=HOURLY', /* every minute /
   repeat_interval    =>  'FREQ=HOURLY;INTERVAL=1',-- / every other month */
   end_date           =>  '31-DEC-2021 04.00.00 AM GMT',
   auto_drop          =>   FALSE,
   enabled          =>   true,
   comments           =>  'calculate rates on all accounts');
END;
/

--Booking job
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'booking_transactions', --name of job
   job_type           =>  'STORED_PROCEDURE', -- type
   job_action         =>  'bookkeeping2', -- procude name
   start_date         =>   SYSTIMESTAMP, -- byrjar frá tá ið hon verður gjørd
   repeat_interval    =>  'FREQ=MINUTELY;Interval=30', /* every minute /
   --repeat_interval    =>  'FREQ=MONTH;INTERVAL=1', / every other month */
   end_date           =>  '31-DEC-2021 04.00.00 AM GMT',
   auto_drop          =>   FALSE,
   enabled          =>   true,
   comments           =>  'Add area51 stuff');
END;
/

-- Insert data ----------------------------------------------------------------------
call INITIALIZEBANK('Hans', 'Lærarin', 'Blaasvær', 100, '01-JAN-1970', 'Doge Bank', 10000000);
call CREATECLIENTS();
INSERT INTO BOOKINGTRANSACTIONS_DATA VALUES (BOOK_TRANSACTIONS_SEQUENCE.nextval, TO_TIMESTAMP('2020-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));

-- commit ----------------------------------------------------------------------
commit;