-- Create functions ----------------------------------------------------------------------
create or replace FUNCTION calcRate(
    fromDate_p  DATE,
    toDate_p  DATE,
    rateFactor NUMBER)
  RETURN NUMBER
AS
  -- Fyrst nakrir temp variablar til lokala utrokning
  sum_account NUMBER(12,2);
  sum_total   NUMBER(12,2);
  temp_saldo  NUMBER(12,2);
  temp_date   DATE;
  bank_account_equity NUMBER;
  bank_account_balance NUMBER;
  -- So ein cursara til at renna ígjøgnum kontonummur
  CURSOR accountCursor IS
  SELECT a.* 
  FROM account a 
  ORDER BY a.account_number; -- riggar
  -- Og ein cursara til at renna ígjøgnum bókingar á hvørjari konto
  CURSOR transCursor(c_from DATE, c_to DATE , c_account VARCHAR2) IS
    SELECT t.*
    FROM bookedtransaction t
    WHERE t.transactionDate > c_from
    AND t.transactionDate  <= c_to
    AND t.account_number    = c_account
    ORDER BY t.transactionDate;

BEGIN
  SELECT account_number INTO bank_account_equity FROM account WHERE clientid = 1 AND type = 89;
  SELECT account_number INTO bank_account_balance FROM account WHERE clientid = 1 AND type = 88;
  --dbms_output.put('sum total ');
  --set serveroutput on
  --begin   dbms_output.put_line('dbms_output.put_line(''soem text...'');');end;
  -- nullstilla sum
  sum_total :=0;
  -- renn ígjøgnum allar konti
  FOR accountRecords IN accountCursor LOOP
    IF accountRecords.account_number = bank_account_equity OR accountRecords.account_number = bank_account_balance THEN
      CONTINUE;
    END IF;
    -- nullstilla sum og frá dato, so hetta byrjar umaftur fyri hvørja konto
    sum_account := 0;
    temp_date   := fromDate_p;
    -- finn byrjunarsaldo fyri tíðarskeið/konto
    SELECT NVL(SUM(t.amount),0) INTO temp_saldo
    FROM transaction t
    WHERE t.account_number  = accountRecords.account_number and
          t.transactionDate <= fromDate_p;
    -- renn ígjøgnum bókingar
    FOR transRecords IN transCursor(fromDate_p, toDate_p, accountRecords.account_number) LOOP
      -- rokna rentu
      sum_account := sum_account + (trunc(transRecords.transactionDate) - temp_date) * temp_saldo * rateFactor/360;
      -- dagfør saldo og dagfesting til næsta umfar
      temp_saldo  := temp_saldo + transRecords.amount;
      temp_date   := trunc(transRecords.transactionDate);
    END LOOP;
    -- rokna rentu um har eru dagar eftir í mánaðinum
    IF temp_date   < toDate_p THEN
      sum_account := sum_account + (toDate_p - temp_date) * temp_saldo * rateFactor/360;
    END IF;
    -- bóka rentu á konto
    dbms_output.put('Rate of ' || accountRecords.account_number || ': ' || sum_account);
    IF sum_account != 0 THEN
      INSERT INTO transaction
        --( transactiondate,  account_number, amount)
        (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, cid)
        VALUES
        --(toDate_p, accountRecords.account_number, sum_account);
        (TRANSACTION_ID_SEQ.nextval, TRANSACTION_PAIR_ID_SEQ.nextval, 1010, accountRecords.account_number, sysdate, sysdate, 'Renta', sum_account, 1);
    END IF;
    --dagfør total
    sum_total := sum_total + sum_account;
  END LOOP;
  -- returnera total til kallara
  RETURN sum_total;
  dbms_output.put('sum total ' || sum_total);
END;
/

create or replace FUNCTION CREATEACCOUNT2  
(
  overdraw IN NUMBER
, clientid IN NUMBER
, accounttype IN NUMBER
, name IN VARCHAR2
) RETURN NUMBER IS
next_account NUMBER;
tvorsum number;
rest number;
acc varchar2(11);
accnumber NUMBER;
acctype VARCHAR2(6);
bankreg VARCHAR2(4);
BEGIN
loop 
      select account_number.nextval into next_account from dual;
      -- skrásetingarnummar er 6460 og kontonummar byrjar við 41 fyri vanligar konti
      -- síðan kemur eitt raðtal frá teljaranum
      acctype := concat('6460', to_char(accounttype));
      acc := concat(acctype, lpad(to_char(next_account), 4, '0')) ;
      tvorsum := 5 * to_number(substr(acc, 1, 1)) +
                 4 * to_number(substr(acc, 2, 1)) +
                 3 * to_number(substr(acc, 3, 1)) +
                 2 * to_number(substr(acc, 4, 1)) +
                 7 * to_number(substr(acc, 5, 1)) +
                 6 * to_number(substr(acc, 6, 1)) +
                 5 * to_number(substr(acc, 7, 1)) +
                 4 * to_number(substr(acc, 8, 1)) +
                 3 * to_number(substr(acc, 9, 1)) +
                 2 * to_number(substr(acc, 10, 1));
      rest := 11 - mod(tvorsum,11);
      exit when rest < 10;  -- endurtak um rest er 10
    end loop;
   accnumber := to_number(acc) * 10 + rest;
  INSERT INTO account VALUES (clientid, 0, -overdraw, accounttype, accnumber, name);
  --INSERT INTO clientaccount VALUES (ACCOUNT_ID_SEQUENCE.currval, clientid);
  return accnumber;
END CREATEACCOUNT2;
/

create or replace FUNCTION ISEMPLOYEE (
  clientid NUMBER
)
RETURN BOOLEAN AS
  isEmployeeSize NUMBER;
  isEmployee BOOLEAN;
BEGIN
  SELECT COUNT(CLIENTID) INTO isEmployeeSize FROM EMPLOYEE;
  IF isEmployeeSize > 0 THEN
    isEmployee := true;
  END IF;
  RETURN isEmployee;
END ISEMPLOYEE;
/