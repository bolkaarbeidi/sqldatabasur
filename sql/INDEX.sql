CREATE INDEX idx_zipcode_client ON client(zipcode);

CREATE INDEX idx.account_type ON account(type);

CREATE INDEX idx.employee_clientid ON employee(clientid);

CREATE INDEX idx.child ON child(childid);

CREATE INDEX idx.parent ON parent(parentid);

CREATE INDEX idx.partner1 ON partner(parent1);

CREATE INDEX idx.partner2 ON partner(parent2);

Create INDEX idx.trans_accountidout ON transaction(accountidout);

Create INDEX idx.bookedtrans_accountidout ON bookedTransaction(accountidout);