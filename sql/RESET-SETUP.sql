-- Dropping tables in correct order
drop table employee;
drop table child;
drop table partner;
drop table registration;
drop table transaction;
drop table bookedTransaction;
drop table account;
drop table accounttype;
drop table rateCalculations;
drop table client;
drop table zip;

-- Drops sequences
DROP SEQUENCE account_number;
DROP SEQUENCE client_id_seq;
DROP SEQUENCE transaction_id_seq;
DROP SEQUENCE transaction_pair_id_seq;

-- Creating tables in correct order
create table zip (
  zipcode NUMBER(20) not null,
  city VARCHAR2(60) not null,
  primary key (zipcode)
);


create table client (
  firstname VARCHAR2(20) not null,
  middlename VARCHAR2(255),
  lastname VARCHAR2(40) not null,
  clientid NUMBER(10),
  zipcode NUMBER(20) not null,
  dob DATE not null,
  primary key(clientid),
  foreign key (zipcode) references zip (zipcode)
);

create table child (
  childid NUMBER(10) NOT NULL,
  parentid NUMBER(10) NOT NULL,
  primary key (childid, parentid),
  foreign key (childid) references client (clientid),
  foreign key (parentid) references client (clientid)
);

create table partner (
  partner1 NUMBER(10) not null,
  partner2 NUMBER(10) not null,
  primary key (partner1, partner2),
  foreign key (partner1) references client (clientid),
  foreign key (partner2) references client (clientid)
);


create table employee (
  role VARCHAR2(30) not null,
  salary NUMBER(20) not null,
  employeeid NUMBER(10),
  clientid NUMBER(10) not null,
  primary key(employeeid),
  foreign key (clientid) references client (clientid)
);


create table registration (
  name VARCHAR2(20) not null,
  bankid NUMBER(20),
  balance NUMBER(36) not null,
  equity NUMBER(36) not null,
  primary key (bankid),
  check ((balance + equity) = 0)
  --check (balance > 0),
  --check (equity > 0)
);


create table accounttype (
  type NUMBER(2) not null,
  credit NUMBER(36) not null,
  debit NUMBER(36) not null,
  debitrates NUMBER(20) not null,
  creditrates NUMBER(20) not null,
  overdrawrates NUMBER(20) not null,
  primary key (type),
  CHECK (type BETWEEN 10 AND 99)
);


create table account (
  clientid NUMBER(10) not null,
  balance NUMBER(36) NOT NULL,
  maxoverdraw NUMBER(20) NOT NULL,
  type NUMBER(2) NOT NULL,
  account_number NUMBER(11),
  name VARCHAR2(20) NOT NULL,
  primary key (account_number),
  foreign key (type) references accounttype (type),
  foreign key (clientid) references client (clientid)
);


create table transaction (
  transactiontype NUMBER(5) NOT NULL,
  account_number_out NUMBER(20) NOT NULL,
  account_number_in NUMBER(20) NOT NULL,
  transactionid NUMBER(36) NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  primary key(transactionid),
  foreign key (account_number_out) references account (account_number),
  foreign key (account_number_in) references account (account_number)
);


CREATE TABLE bookedTransaction (
  transactiontype NUMBER(5) NOT NULL,
  account_number_out NUMBER(20) NOT NULL,
  account_number_in NUMBER(20) NOT NULL,
  transactionid NUMBER(36) NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  primary key(transactionid),
  foreign key (account_number_out) references account (account_number),
  foreign key (account_number_in) references account (account_number)
);

CREATE TABLE rateCalculations (	
  fromDate DATE NOT NULL, 
  toDate DATE NOT NULL, 
  sumRate NUMBER(12,2), 
  ratePercent NUMBER(2,0), 
  PRIMARY KEY (fromDate)
);

-- Creates procedures
create or replace PROCEDURE CREATECLIENT
(
  FIRSTNAME IN VARCHAR2 
, MIDDLENAME IN VARCHAR2 
, LASTNAME IN VARCHAR2 
, ZIPCODE IN NUMBER 
, DOB IN DATE
) AS 
BEGIN
  INSERT INTO client VALUES (FIRSTNAME, MIDDLENAME, LASTNAME, CLIENT_ID_SEQ.nextval, ZIPCODE, DOB);
END CREATECLIENT;

-- Creates procedures
create or replace PROCEDURE CREATEACCOUNT 
(
  overdraw IN NUMBER
, clientid IN NUMBER
, accounttype IN NUMBER
, name IN VARCHAR2
) IS
next_account NUMBER;
tvorsum number;
rest number;
acc varchar2(11);
accnumber NUMBER;
acctype VARCHAR2(6);
bankreg VARCHAR2(4);
BEGIN
loop 
      select accountnum_seq.nextval into next_account from dual;
      -- skrásetingarnummar er 6460 og kontonummar byrjar við 41 fyri vanligar konti
      -- síðan kemur eitt raðtal frá teljaranum
      acctype := concat('6460', to_char(accounttype));
      acc := concat(acctype, lpad(to_char(next_account), 4, '0')) ;
      tvorsum := 5 * to_number(substr(acc, 1, 1)) +
                 4 * to_number(substr(acc, 2, 1)) +
                 3 * to_number(substr(acc, 3, 1)) +
                 2 * to_number(substr(acc, 4, 1)) +
                 7 * to_number(substr(acc, 5, 1)) +
                 6 * to_number(substr(acc, 6, 1)) +
                 5 * to_number(substr(acc, 7, 1)) +
                 4 * to_number(substr(acc, 8, 1)) +
                 3 * to_number(substr(acc, 9, 1)) +
                 2 * to_number(substr(acc, 10, 1));
      rest := 11 - mod(tvorsum,11);
      exit when rest < 10;  -- endurtak um rest er 10
    end loop;
   accnumber := to_number(acc) * 10 + rest;
  INSERT INTO account VALUES (clientid, 0, overdraw, accounttype, accnumber, name);
  --INSERT INTO clientaccount VALUES (ACCOUNT_ID_SEQUENCE.currval, clientid);
END CREATEACCOUNT;

create or replace PROCEDURE CREATEBANK 
(
  NAME IN VARCHAR2 
, START_MONEY IN NUMBER
, ACCOUNT_BALANCE IN NUMBER 
, ACCOUNT_EQUITY IN NUMBER
, BANKID IN NUMBER
) AS 
BEGIN
  --INSERT INTO registration VALUES (name, bankid, -start_money, start_money, account_balance, account_equity);
  INSERT INTO registration VALUES (name, bankid, -start_money, start_money);
END CREATEBANK;

create or replace PROCEDURE createTransaction 
(
  -- Input parameters
  account_number_out IN NUMBER,
  account_number_in IN NUMBER,
  transactiontype in NUMBER,
  message in VARCHAR2,
  amount in NUMBER
) IS
amount2 NUMBER;
BEGIN
  INSERT INTO transaction VALUES (transactiontype, account_number_out, account_number_in, TRANSACTION_ID_SEQ.nextval, message, -amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.nextval);
  INSERT INTO transaction VALUES (transactiontype, account_number_in, account_number_out, TRANSACTION_ID_SEQ.nextval, message, amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.nextval);
END createTransaction;

-- Procedures part done



-- Creates sequences
CREATE SEQUENCE account_number MINVALUE 1 MAXVALUE 99999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE client_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 13 START WITH 2002 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE transaction_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 3 START WITH 873 CACHE 20 NOORDER  NOCYCLE ;
CREATE SEQUENCE transaction_pair_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 3 START WITH 317 CACHE 20 NOORDER  NOCYCLE ;

-- Inserts post numbers:
insert into Zip values (927, 'Akrar');
insert into Zip values (160, 'Argir');
insert into Zip values (165, 'Argir (postsmoga)');
insert into Zip values (726, 'Ánir');
insert into Zip values (727, 'Árnafjørður');
insert into Zip values (386, 'Bøur');
insert into Zip values (235, 'Dalur');
insert into Zip values (735, 'Depil');
insert into Zip values (470, 'Eiði');
insert into Zip values (478, 'Elduvík');
insert into Zip values (870, 'Fámjin');
insert into Zip values (825, 'Froðba');
insert into Zip values (530, 'Fuglafjørður');
insert into Zip values (477, 'Funningsfjørður');
insert into Zip values (475, 'Funningur');
insert into Zip values (387, 'Gásadalur');
insert into Zip values (476, 'Gjógv');
insert into Zip values (625, 'Glyvrar');
insert into Zip values (510, 'Gøta');
insert into Zip values (666, 'Gøtueiði');
insert into Zip values (511, 'Gøtugjógv');
insert into Zip values (440, 'Haldarsvík');
insert into Zip values (785, 'Haraldssund');
insert into Zip values (767, 'Hattarvík');
insert into Zip values (695, 'Hellur');
insert into Zip values (280, 'Hestur');
insert into Zip values (420, 'Hósvík');
insert into Zip values (960, 'Hov');
insert into Zip values (188, 'Hoyvík');
insert into Zip values (796, 'Húsar');
insert into Zip values (230, 'Húsavík');
insert into Zip values (850, 'Hvalba');
insert into Zip values (430, 'Hvalvík');
insert into Zip values (740, 'Hvannasund');
insert into Zip values (187, 'Hvítanes');
insert into Zip values (494, 'Innan Glyvur');
insert into Zip values (180, 'Kaldbak');
insert into Zip values (185, 'Kaldbaksbotnur');
insert into Zip values (766, 'Kirkja');
insert into Zip values (175, 'Kirkjubøur');
insert into Zip values (700, 'Klaksvík');
insert into Zip values (495, 'Kolbanargjógv');
insert into Zip values (410, 'Kollafjørður');
insert into Zip values (285, 'Koltur');
insert into Zip values (780, 'Kunoy');
insert into Zip values (340, 'Kvívík');
insert into Zip values (626, 'Lambareiði');
insert into Zip values (627, 'Lambi');
insert into Zip values (438, 'Langasandur');
insert into Zip values (520, 'Leirvík');
insert into Zip values (335, 'Leynar');
insert into Zip values (466, 'Ljósá');
insert into Zip values (926, 'Lopra');
insert into Zip values (370, 'Miðvágur');
insert into Zip values (797, 'Mikladalur');
insert into Zip values (496, 'Morskranes');
insert into Zip values (737, 'Múli');
insert into Zip values (388, 'Mykines');
insert into Zip values (655, 'Nes (Eysturoy)');
insert into Zip values (925, 'Nes (Vágur)');
insert into Zip values (437, 'Nesvík');
insert into Zip values (730, 'Norðdepil');
insert into Zip values (725, 'Norðoyri');
insert into Zip values (178, 'Norðradalur');
insert into Zip values (512, 'Norðragøta');
insert into Zip values (460, 'Norðskáli');
insert into Zip values (736, 'Norðtoftir');
insert into Zip values (270, 'Nólsoy');
insert into Zip values (690, 'Oyndarfjørður');
insert into Zip values (400, 'Oyrarbakki');
insert into Zip values (415, 'Oyrareingir');
insert into Zip values (450, 'Oyri');
insert into Zip values (950, 'Porkeri');
insert into Zip values (640, 'Rituvík');
insert into Zip values (620, 'Runavík');
insert into Zip values (436, 'Saksun');
insert into Zip values (600, 'Saltangará');
insert into Zip values (656, 'Saltnes');
insert into Zip values (360, 'Sandavágur');
insert into Zip values (210, 'Sandur');
insert into Zip values (860, 'Sandvík');
insert into Zip values (497, 'Selatrað');
insert into Zip values (416, 'Signabøur');
insert into Zip values (236, 'Skarvanes');
insert into Zip values (485, 'Skálabotnur');
insert into Zip values (220, 'Skálavík');
insert into Zip values (480, 'Skáli');
insert into Zip values (665, 'Skipanes');
insert into Zip values (240, 'Skopun');
insert into Zip values (260, 'Skúvoy');
insert into Zip values (336, 'Skælingur');
insert into Zip values (286, 'Stóra Dímun');
insert into Zip values (490, 'Strendur');
insert into Zip values (435, 'Streymnes');
insert into Zip values (330, 'Stykkið');
insert into Zip values (970, 'Sumba');
insert into Zip values (186, 'Sund');
insert into Zip values (465, 'Svínáir');
insert into Zip values (765, 'Svínoy');
insert into Zip values (795, 'Syðradalur (Kal.)');
insert into Zip values (177, 'Syðradalur (Str.)');
insert into Zip values (513, 'Syðrugøta');
insert into Zip values (660, 'Søldarfjørður');
insert into Zip values (380, 'Sørvágur');
insert into Zip values (445, 'Tjørnuvík');
insert into Zip values (650, 'Toftir');
insert into Zip values (100, 'Tórshavn');
insert into Zip values (110, 'Tórshavn (postsmoga)');
insert into Zip values (826, 'Trongisvágur');
insert into Zip values (798, 'Trøllanes');
insert into Zip values (800, 'Tvøroyri');
insert into Zip values (900, 'Vágur');
insert into Zip values (358, 'Válur');
insert into Zip values (385, 'Vatnsoyrar');
insert into Zip values (176, 'Velbastaður');
insert into Zip values (350, 'Vestmanna');
insert into Zip values (750, 'Viðareiði');
insert into Zip values (928, 'Víkarbyrgi');
insert into Zip values (645, 'Æðuvík');
insert into Zip values (827, 'Øravík');

