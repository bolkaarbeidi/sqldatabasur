call CREATECLIENT('James', '', 'Potter', 100, '27-MAR-1960', '270360-111');
call CREATECLIENT('Lily', 'J', 'Potter', 100, '30-JAN-1960', '300160-220');
call CREATECLIENT('Harry', 'James', 'Potter', 100, '31-JUL-1980', '310780-111');


-- client numbers may not be correct:
call CREATECHILD(8, 6);
call CREATECHILD(8, 7);

call CREATEPARTNER(7, 6);
call CREATEPARTNER(6, 7);