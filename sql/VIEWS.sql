CREATE OR REPLACE VIEW accounts_summary (transaction, account) AS
SELECT A.account_number, SUM(T.amount) FROM transaction T
INNER JOIN account A ON T.account_number = A.account_number GROUP BY A.account_number;

--CREATE OR REPLACE VIEW transactions_view (message, account_number, amount, account_number_from)
--AS SELECT message, account_number_in, amount, account_number_out
--FROM transaction;

CREATE OR REPLACE VIEW CLIENTRELATION (CLIENTID, FIRSTNAME, LASTNAME, PARTNERID) AS 
select c.clientid, c.firstname, c.lastname, p.partner2 as partnerID
from client c, partner p
where c.CLIENTID = p.PARTNER1
  UNION
select c.clientid, c.firstname, c.lastname, p.partner1
from client c, partner p
where c.CLIENTID = p.PARTNER2
  Union
select c.clientid, c.firstname, c.lastname, d.childid
from client c, CHILD d
where c.CLIENTID = d.PARENTID;

--CREATE OR REPLACE VIEW transaction_pairs (transactionid, transactionpair, transactiontype, account_number_in, transactiondate, valuedate, message, amount_in, account_number_out, amount_out) AS
--SELECT t1.*, t2.account_number, t2.amount FROM transaction t1 LEFT JOIN transaction t2 ON t1.transactionpair = t2.transactionpair AND t1.transactionid != t2.transactionid;

CREATE OR REPLACE VIEW transaction_summary (account_number, valuedate, message, amount, name) AS
SELECT T1.account_number, T1.valuedate, T1.message, T1.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name FROM transaction T1 LEFT JOIN account a ON T1.account_number = a.account_number LEFT JOIN client c ON a.clientid = c.clientid;

CREATE OR REPLACE VIEW partner_accounts AS
SELECT c.firstname, c.middlename, c.lastname, c.clientid, p.partner1 personlink, a.balance, a.account_number, a.maxoverdraw, a.type, a.name FROM client c LEFT JOIN partner p ON c.clientid = p.partner2 LEFT JOIN account a ON a.clientid = p.partner2;

CREATE OR REPLACE VIEW child_accounts AS
SELECT c.firstname, c.middlename, c.lastname, c.clientid, p.parentid personlink, a.balance, a.account_number, a.maxoverdraw, a.type, a.name FROM client c LEFT JOIN child p ON c.clientid = p.childid LEFT JOIN account a ON a.clientid = p.childid;



CREATE OR REPLACE VIEW ACCOUNT_TRANSACTIONS (ACCOUNT_NUMBER, TRANSACTIONDATE, MESSAGE, AMOUNT) AS 
SELECT account_number, valuedate, message, amount
FROM transaction;
 

--CREATE OR REPLACE VIEW TRANSACTION_PAIRS2 (TRANSACTIONID, TRANSACTIONPAIR, TRANSACTIONTYPE, ACCOUNT_NUMBER, TRANSACTIONDATE, VALUEDATE, MESSAGE, AMOUNT, ACCOUNT_NUMBER_1, AMOUNT_1) AS 
--SELECT t1.*, t2.account_number, t2.amount FROM transaction t1 LEFT JOIN transaction t2 ON t1.transactionpair = t2.transactionpair AND t1.transactionid != t2.transactionid;
 

CREATE OR REPLACE VIEW all_transactions_view (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, status) AS
SELECT bt.transactionid, bt.transactionpair, bt.transactiontype, bt.account_number, bt.transactiondate, bt.valuedate, bt.message, bt.amount, 'b' status FROM bookedtransaction bt
UNION
SELECT t.transactionid, t.transactionpair, t.transactiontype, t.account_number, t.transactiondate, t.valuedate, t.message, t.amount, 'P' status FROM transaction t;
 
CREATE OR REPLACE VIEW all_transactions_view_reduced (account_number, transactiondate, message, ammount, status) AS
SELECT account_number, valuedate, message, amount, status FROM ALL_TRANSACTIONS_VIEW;

CREATE OR REPLACE VIEW all_transactions (account_number, valuedate, message, amount, clientid, name, status) AS
SELECT bt.account_number, bt.valuedate, bt.message, bt.amount, a.clientid, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name, 'b' status FROM bookedtransaction bt inner join account a on a.account_number = bt.account_number inner join client c on c.clientid = a.clientid
union
SELECT t.account_number, t.valuedate, t.message, t.amount, a.clientid, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name, 'p' status FROM transaction t inner join account a on a.account_number = t.account_number inner join client c on c.clientid = a.clientid;


CREATE OR REPLACE VIEW transactions_all_uber (account_number, valuedate, message, amount, name, status, clientid, transactionpair) AS
SELECT distinct t2.account_number, t2.valuedate, t2.message, t1.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' as status, c.clientid, t1.transactionpair
FROM bookedtransaction t1
JOIN bookedtransaction t2
ON t1.transactionpair = t2.transactionpair
JOIN account a
ON t2.ACCOUNT_NUMBER = a.ACCOUNT_NUMBER
JOIN client c
ON a.clientid = c.clientid
WHERE t1.transactionpair = t1.transactionpair and t1.transactiontype != 1000 AND t1.account_number != t2.account_number
--WHERE t1.account_number = 64601400039 and t2.account_number != 64601400039 and t1.transactionpair = t1.transactionpair and t1.transactiontype != 1000
union
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' as status, c.clientid, t.transactionpair
FROM bookedtransaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid
WHERE t.transactiontype = 1000;


CREATE OR REPLACE VIEW transaction_summary2 (account_number, valuedate, message, amount, name, status) AS
SELECT * FROM (
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'p' status
FROM transaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid
UNION
SELECT t.account_number, t.valuedate, t.message, t.amount, c.firstname || ' ' || c.middlename || ' ' || c.lastname name, 'b' status
FROM bookedtransaction t
INNER JOIN account a
ON t.account_number = a.account_number
INNER JOIN client c
ON a.clientid = c.clientid)
ORDER BY valuedate desc;


CREATE OR REPLACE VIEW PARTNER_ACCOUNTS2 (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT p.partner1, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name
FROM partner p
INNER JOIN account a
ON a.clientid = p.partner2;


CREATE OR REPLACE VIEW CHILD_ACCOUNTS2 (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT c.parentid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name
FROM child c
INNER JOIN account a
ON a.clientid = c.childid;


CREATE OR REPLACE VIEW CHILD_AND_PARTNER_ACCOUNTS (clientid, partnerid, balance, maxoverdraw, type, account_number, name)
AS
SELECT * FROM (
SELECT * FROM PARTNER_ACCOUNTS2
UNION
SELECT * FROM CHILD_ACCOUNTS2
);

CREATE OR REPLACE VIEW PARTNER_ACCOUNTS3 (clientid, personlinkid, balance, maxoverdraw, type, account_number, accountname, name)
AS
SELECT p.partner1, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c.firstname || ' ' || c.middlename || ' ' || c.lastname as name
FROM partner p
INNER JOIN account a
ON a.clientid = p.partner2
INNER JOIN client c
ON p.partner2 = c.clientid;

CREATE OR REPLACE VIEW CHILD_ACCOUNTS3 (clientid, personlinkid, balance, maxoverdraw, type, account_number, accountname, name)
AS
SELECT c.parentid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c1.firstname || ' ' || c1.middlename || ' ' || c1.lastname as name
FROM child c
INNER JOIN account a
ON a.clientid = c.childid
INNER JOIN client c1
ON c.childid = c1.clientid;

CREATE OR REPLACE VIEW my_own_accounts (clientid, personlinkid, balance, maxoverdraw, type, account_number, account_name, name) AS
SELECT c.clientid, a.clientid, a.balance, a.maxoverdraw, a.type, a.account_number, a.name, c.firstname || ' ' || c.middlename || ' ' || c.lastname name FROM account a
INNER JOIN client c
ON a.clientid = c.clientid;

CREATE OR REPLACE VIEW COUPLE_TOTAL_BALANCE(CLIENTID, COUPLE_BALANCE) AS 
  select CLIENTID, COUPLE_BALANCE from 
   (Select clientid, sum(balance) as Couple_Balance
   from COUPLE_ACCOUNTS
   group by CLIENTID);

CREATE OR REPLACE VIEW FAMILY_ACCOUNTS ("CLIENTID", "PERSONLINKID", "BALANCE", "MAXOVERDRAW", "TYPE", "ACCOUNT_NUMBER", "ACCOUNTNAME", "NAME") AS 
  select "CLIENTID","PERSONLINKID","BALANCE","MAXOVERDRAW","TYPE","ACCOUNT_NUMBER","ACCOUNTNAME","NAME" from (
select * from partner_accounts3
union
select * from child_accounts3
union
select * from my_own_accounts);