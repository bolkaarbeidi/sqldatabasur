-- Dropping jobs:
BEGIN
  dbms_scheduler.drop_job(job_name => 'BOOKING_TRANSACTIONS');
  dbms_scheduler.drop_job(job_name => 'CALCULATING_RATES');
END;

-- Booked transactions
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'booking_transactions', --name of job
   job_type           =>  'STORED_PROCEDURE', -- type
   job_action         =>  'bookkeeping2', -- procude name
   start_date         =>   SYSTIMESTAMP, -- byrjar frá tá ið hon verður gjørd
   repeat_interval    =>  'FREQ=MINUTELY;INTERVAL=15', /* every minute /
   --repeat_interval    =>  'FREQ=MONTH;INTERVAL=1', / every other month */
   end_date           =>  '31-DEC-2021 04.00.00 AM GMT',
   auto_drop          =>   FALSE,
   enabled          =>   true,
   comments           =>  'Add area51 stuff');
END;

-- Rate calculations
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
   job_name           =>  'calculating_rates', --name of job
   job_type           =>  'STORED_PROCEDURE', -- type
   job_action         =>  'calculaterate', -- procude name
   start_date         =>   SYSTIMESTAMP, -- byrjar frá tá ið hon verður gjørd
   --repeat_interval    =>  'FREQ=HOURLY', /* every minute /
   repeat_interval    =>  'FREQ=MONTHLY;INTERVAL=1', --/ every other month */
   end_date           =>  '31-DEC-2021 04.00.00 AM GMT',
   auto_drop          =>   FALSE,
   enabled          =>   true,
   comments           =>  'calculate rates on all accounts');
END;