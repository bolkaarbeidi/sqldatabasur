-- Create triggers ----------------------------------------------------------------------
-- rateCalculation (renturoknin)
CREATE OR REPLACE TRIGGER CALCRATE_INS before insert on rateCalculation
for each row
begin
  select max(toDate)+1 into :new.fromDate
  from rateCalculation;
  :new.sumrate := calcRate(:new.fromDate, :new.toDate, :new.ratepercent/100);
end;
/

-- Update balance on account when inserting into transaction table
CREATE OR REPLACE TRIGGER UPDATE_BALANCE 
BEFORE INSERT ON BOOKEDTRANSACTION 
FOR EACH ROW
DECLARE
new_account_number NUMBER;
new_balance NUMBER;
BEGIN
  new_account_number := :new.account_number;
  SELECT balance INTO new_balance FROM account WHERE account_number = new_account_number;
  new_balance := new_balance + :new.amount;
  UPDATE account
    SET balance = new_balance
    WHERE account_number = new_account_number;
END;
/

-- Ptal check, raises error if invalid ptal is given
CREATE OR REPLACE TRIGGER PTALCHECK 
BEFORE INSERT ON CLIENT
for each row
DECLARE
checkcipher NUMBER;
modcheck NUMBER;
ptalnew VARCHAR2(10);
BEGIN
  SELECT :new.ptal INTO ptalnew from dual;
  DBMS_OUTPUT.put_line('PTAL: ' || ptalnew);
  checkcipher :=   3 * to_number(substr(ptalnew, 1, 1)) +
                   2 * to_number(substr(ptalnew, 2, 1)) +
                   7 * to_number(substr(ptalnew, 3, 1)) +
                   6 * to_number(substr(ptalnew, 4, 1)) +
                   5 * to_number(substr(ptalnew, 5, 1)) +
                   4 * to_number(substr(ptalnew, 6, 1)) +
                   3 * to_number(substr(ptalnew, 8, 1)) +
                   2 * to_number(substr(ptalnew, 9, 1)) +
                   1 * to_number(substr(ptalnew, 10, 1));
  modcheck := mod(checkcipher,11);
  DBMS_OUTPUT.put_line('Modulus check gave: ' || modcheck);
  IF (modcheck != 0) THEN
    raise_application_error(-20000, 'Invalid P-Tal'); 
    --SELECT -1 INTO :new.pid FROM dual;
  END IF;
END;
/