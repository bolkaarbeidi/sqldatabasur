-- Insert data ----------------------------------------------------------------------
call INITIALIZEBANK('Hans', 'Lærarin', 'Blaasvær', 100, '01-JAN-1970', 'Doge Bank', 10000000);
call CREATECLIENTS();
INSERT INTO BOOKINGTRANSACTIONS_DATA VALUES (BOOK_TRANSACTIONS_SEQUENCE.nextval, TO_TIMESTAMP('2020-12-31 00:00:00', 'YYYY-MM-DD HH24:MI:SS'));
INSERT INTO employee VALUES ('Boss', 1000000, 1, 1);
commit;