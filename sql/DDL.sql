-- Dropping tables ----------------------------------------------------------------------
drop table employee;
drop table child;
drop table partner;
drop table registration;
drop table transaction;
drop table bookedTransaction;
drop table account;
drop table accounttype;
drop table client;
drop table zip;
DROP TABLE rateCalculation;
DROP TABLE bookingtransactions_data;

-- Creating tables ----------------------------------------------------------------------
create table zip (
  zipcode NUMBER(20) not null,
  city VARCHAR2(60) not null,
  primary key (zipcode)
);


create table client (
  firstname VARCHAR2(20) not null,
  middlename VARCHAR2(255),
  lastname VARCHAR2(40) not null,
  clientid NUMBER(10),
  zipcode NUMBER(20) not null,
  dob DATE not null,
  ptal VARCHAR(10) not null,
  primary key(clientid),
  foreign key (zipcode) references zip (zipcode)
);

create table child (
  childid NUMBER(10) NOT NULL,
  parentid NUMBER(10) NOT NULL,
  primary key (childid, parentid),
  foreign key (childid) references client (clientid),
  foreign key (parentid) references client (clientid)
);

create table partner (
  partner1 NUMBER(10) not null,
  partner2 NUMBER(10) not null,
  primary key (partner1, partner2),
  foreign key (partner1) references client (clientid),
  foreign key (partner2) references client (clientid)
);


create table employee (
  role VARCHAR2(30) not null,
  salary NUMBER(20) not null,
  employeeid NUMBER(10),
  clientid NUMBER(10) not null,
  primary key(employeeid),
  foreign key (clientid) references client (clientid)
);


create table registration (
  name VARCHAR2(20) not null,
  bankid NUMBER(20),
  balance NUMBER(36,2) not null,
  equity NUMBER(36,2) not null,
  primary key (bankid),
  check ((balance + equity) = 0)
  --check (balance > 0),
  --check (equity > 0)
);


create table accounttype (
  type NUMBER(2) not null,
  credit NUMBER(36) not null,
  debit NUMBER(36) not null,
  debitrates NUMBER(20) not null,
  creditrates NUMBER(20) not null,
  overdrawrates NUMBER(20) not null,
  primary key (type),
  CHECK (type BETWEEN 10 AND 99)
);


create table account (
  clientid NUMBER(10) not null,
  balance NUMBER(36,2) NOT NULL,
  maxoverdraw NUMBER(20,2) NOT NULL,
  type NUMBER(2) NOT NULL,
  account_number NUMBER(11),
  name VARCHAR2(20) not null,
  primary key (account_number),
  foreign key (type) references accounttype (type),
  foreign key (clientid) references client (clientid)
);


create table transaction (
  -- Fields
  transactionid NUMBER(36) NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  transactiontype NUMBER(5) NOT NULL,
  account_number NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20,2) NOT NULL,
  cid NUMBER NOT NULL,
  --account_number_out NUMBER(20) NOT NULL,
  --account_number_in NUMBER(20) NOT NULL,

  -- Constraints
  primary key(transactionid),
  foreign key (account_number) references account (account_number),
  foreign key (cid) references client (clientid)
  --foreign key (account_number_out) references account (account_number),
  --foreign key (account_number_in) references account (account_number)
);


CREATE TABLE bookedTransaction (
  -- Fields
  transactionid NUMBER(36) NOT NULL,
  transactionpair NUMBER(36) NOT NULL,
  transactiontype NUMBER(5) NOT NULL,
  account_number NUMBER(20) NOT NULL,
  transactiondate TIMESTAMP NOT NULL,
  valuedate TIMESTAMP NOT NULL,
  message VARCHAR2(255) DEFAULT '',
  amount NUMBER(20,2) NOT NULL,
  cid NUMBER NOT NULL,
  --account_number_out NUMBER(20) NOT NULL,
  --account_number_in NUMBER(20) NOT NULL,

  -- Constraints
  primary key(transactionid),
  foreign key (account_number) references account (account_number),
  foreign key (cid) references client (clientid)
  --foreign key (account_number_out) references account (account_number),
  --foreign key (account_number_in) references account (account_number)
);

CREATE TABLE rateCalculation (	
  fromDate DATE NOT NULL, 
  toDate DATE NOT NULL, 
  sumRate NUMBER(12,2), 
  ratePercent NUMBER(2,0), 
  PRIMARY KEY (fromDate)
);

CREATE TABLE bookingtransactions_data (
  id NUMBER,
  last_booking TIMESTAMP
);

