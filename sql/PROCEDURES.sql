-- Creating procedures ----------------------------------------------------------------------
-- Transaction procedure
create or replace PROCEDURE createTransaction 
(
  -- Input parameters
  account_number_out IN NUMBER,
  account_number_in IN NUMBER,
  message in VARCHAR2,
  amount in NUMBER
  cid in NUMBER
) IS
new_balance NUMBER;
pairid NUMBER;
current_balance NUMBER;
limit NUMBER;
account_type NUMBER;
BEGIN
  DBMS_OUTPUT.put_line('test');
  IF (amount >= 0) THEN
    
    SELECT balance INTO current_balance FROM account WHERE account_number = account_number_out;
    current_balance := current_balance - amount;
    SELECT type INTO account_type FROM account WHERE account_number = account_number_out;
    IF (account_type = 15) THEN
      limit := 0;
    ELSE
      SELECT maxoverdraw INTO limit FROM account WHERE account_number = account_number_out;
      
    END IF;
    
    IF (current_balance >= limit) THEN
      DBMS_OUTPUT.put_line('inner test');
      --INSERT INTO transaction VALUES (1002, account_number_out, account_number_in, TRANSACTION_ID_SEQ.nextval, message, amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.nextval);
      --INSERT INTO transaction VALUES (1002, account_number_in, account_number_out, TRANSACTION_ID_SEQ.nextval, message, -amount, SYSDATE, SYSDATE, TRANSACTION_PAIR_ID_SEQ.currval);
      SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO pairid FROM dual;
      INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, pairid, 1002, account_number_out, SYSDATE, SYSDATE, message, -amount, cid);
      INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, pairid, 1002, account_number_in, SYSDATE, SYSDATE, message, amount, cid);
      
      --SELECT balance INTO new_balance FROM account WHERE account_number = account_number_out;
      --UPDATE account
      --SET balance = balance - amount
      --WHERE account_number = account_number_out;
      --SELECT balance INTO new_balance FROM account WHERE account_number = account_number_in;
      --UPDATE account
      --SET balance = balance + amount
      --WHERE account_number = account_number_in;
    END IF;
  END IF;
END createTransaction;
/

-- Create account procedure
create or replace PROCEDURE CREATEACCOUNT 
(
  overdraw IN NUMBER
, clientid IN NUMBER
, accounttype IN NUMBER
, name IN VARCHAR2
) IS
next_account NUMBER;
tvorsum number;
rest number;
acc varchar2(11);
accnumber NUMBER;
acctype VARCHAR2(6);
bankreg VARCHAR2(4);
BEGIN
loop 
      select account_number.nextval into next_account from dual;
      -- skrásetingarnummar er 6460 og kontonummar byrjar við 41 fyri vanligar konti
      -- síðan kemur eitt raðtal frá teljaranum
      acctype := concat('6460', to_char(accounttype));
      acc := concat(acctype, lpad(to_char(next_account), 4, '0')) ;
      tvorsum := 5 * to_number(substr(acc, 1, 1)) +
                 4 * to_number(substr(acc, 2, 1)) +
                 3 * to_number(substr(acc, 3, 1)) +
                 2 * to_number(substr(acc, 4, 1)) +
                 7 * to_number(substr(acc, 5, 1)) +
                 6 * to_number(substr(acc, 6, 1)) +
                 5 * to_number(substr(acc, 7, 1)) +
                 4 * to_number(substr(acc, 8, 1)) +
                 3 * to_number(substr(acc, 9, 1)) +
                 2 * to_number(substr(acc, 10, 1));
      rest := 11 - mod(tvorsum,11);
      exit when rest < 10;  -- endurtak um rest er 10
    end loop;
   accnumber := to_number(acc) * 10 + rest;
  INSERT INTO account VALUES (clientid, 0, -overdraw, accounttype, accnumber, name);
  --INSERT INTO clientaccount VALUES (ACCOUNT_ID_SEQUENCE.currval, clientid);
END CREATEACCOUNT;
/

-- Create bank procedure
create or replace PROCEDURE CREATEBANK 
(
  NAME IN VARCHAR2 
, START_MONEY IN NUMBER
--, ACCOUNT_BALANCE IN NUMBER 
--, ACCOUNT_EQUITY IN NUMBER
, BANKID IN NUMBER
) AS 
BEGIN
  --INSERT INTO registration VALUES (name, bankid, -start_money, start_money, account_balance, account_equity);
  INSERT INTO registration VALUES (name, bankid, -start_money, start_money);
END CREATEBANK;
/

create or replace PROCEDURE CREATECHILD
(
 childid IN number, 
 parentid IN number
 ) AS
 BEGIN
  INSERT INTO child values (childid, parentid);
END Createchild;
/

create or replace PROCEDURE CREATECLIENT
(
  FIRSTNAME IN VARCHAR2 
, MIDDLENAME IN VARCHAR2 
, LASTNAME IN VARCHAR2 
, ZIPCODE IN NUMBER 
, DOB IN DATE
, PTAL IN VARCHAR2
) AS 
BEGIN
  INSERT INTO client VALUES (FIRSTNAME, MIDDLENAME, LASTNAME, CLIENT_ID_SEQ.nextval, ZIPCODE, DOB, PTAL);
END CREATECLIENT;
/

-- Create partner procedure
create or replace PROCEDURE CREATEPARTNER
(
 Partner1 IN number, 
 partner2 IN number
 ) AS
 BEGIN
  INSERT INTO partner values (partner1, partner2);
  INSERT INTO partner values (partner2, partner1);
END CREATEPARTNER;
/

create or replace PROCEDURE DEPOSIT 
( 
  AMOUNT IN NUMBER
, ACCOUNTNR IN NUMBER
, MESSAGE IN VARCHAR2
, cid IN NUMBER
) IS
equity2 NUMBER;
balance2 NUMBER;
account_equity NUMBER;
account_balance NUMBER;
transaction_pair NUMBER;
bankclient NUMBER;
BEGIN
  SELECT bankid INTO bankclient FROM registration WHERE rownum = 1;
  SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO transaction_pair FROM dual;
  SELECT equity INTO equity2 FROM registration WHERE bankid = bankclient;
  SELECT balance INTO balance2 FROM registration WHERE bankid = bankclient;
  
  SELECT account_number INTO account_equity FROM account WHERE clientid = bankclient and type=89;
  SELECT account_number INTO account_balance FROM account WHERE clientid = bankclient and type=88;
  
  equity2 := equity2 - amount;
  balance2 := balance2 + amount;
  UPDATE registration SET equity = equity2, balance = balance2 WHERE bankid = 1;
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, accountnr, SYSDATE, SYSDATE, message, amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_equity, SYSDATE, SYSDATE, message, -amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_balance, SYSDATE, SYSDATE, message, amount, cid);
END DEPOSIT;
/

-- Withdraw procedure
create or replace PROCEDURE WITHDRAW  
( 
  AMOUNT IN NUMBER
, ACCOUNTNR IN NUMBER
, MESSAGE IN VARCHAR2
, cid IN NUMBER
) IS
equity2 NUMBER;
balance2 NUMBER;
account_equity NUMBER;
account_balance NUMBER;
transaction_pair NUMBER;
bankclient NUMBER;
BEGIN
  SELECT bankid INTO bankclient FROM registration WHERE rownum = 1;
  SELECT TRANSACTION_PAIR_ID_SEQ.nextval INTO transaction_pair FROM dual;
  SELECT equity INTO equity2 FROM registration WHERE bankid = bankclient;
  SELECT balance INTO balance2 FROM registration WHERE bankid = bankclient;
  
  SELECT account_number INTO account_equity FROM account WHERE clientid = bankclient and type=89;
  SELECT account_number INTO account_balance FROM account WHERE clientid = bankclient and type=88;
  
  equity2 := equity2 + amount;
  balance2 := balance2 - amount;
  UPDATE registration SET equity = equity2, balance = balance2 WHERE bankid = 1;
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, accountnr, SYSDATE, SYSDATE, message, -amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_equity, SYSDATE, SYSDATE, message, amount, cid);
  INSERT INTO transaction VALUES (TRANSACTION_ID_SEQ.nextval, transaction_pair, 1000, account_balance, SYSDATE, SYSDATE, message, -amount, cid); 
END WITHDRAW;
/

create or replace PROCEDURE INITIALIZEBANK(
  fname VARCHAR2
, mname VARCHAR2
, lname VARCHAR2
, postnumber NUMBER
, dob DATE
, bankname VARCHAR2
, startmoney NUMBER
)
IS
bankclient NUMBER;
equity_account NUMBER;
balance_account NUMBER;
BEGIN
  -- Ger konto typur
  -- Equity (Bank)
  INSERT INTO accounttype VALUES (89, 0, 0, 0, 0, 0);
  -- Balance (Bank)
  INSERT INTO accounttype VALUES (88, 0, 0, 0, 0, 0);
  -- Credit
  INSERT INTO accounttype VALUES (14, 0, 0, 0, 0, 0);
  -- Debit
  INSERT INTO accounttype VALUES (15, 0, 0, 0, 0, 0);
  -- Other
  INSERT INTO accounttype VALUES (16, 0, 0, 0, 0, 0);
  
  CREATECLIENT(fname, mname, lname, postnumber, dob, '111111-111');
  SELECT clientid INTO bankclient FROM client WHERE firstname = fname;
  CREATEEMPLOYEE('Boss', 1000, bankclient);  
  equity_account := CREATEACCOUNT2(startmoney, bankclient, 88, 'EQUITY');
  balance_account := CREATEACCOUNT2(startmoney, bankclient, 89, 'BALANCE');
  CREATEBANK(bankname, startmoney, bankclient);
  UPDATE account
    SET balance = -startmoney
    WHERE account_number = equity_account;
  
  UPDATE account
    SET balance = startmoney
    WHERE account_number = balance_account;
  
END INITIALIZEBANK;
/

create or replace PROCEDURE CREATECLIENTS IS 
tempclientid NUMBER;
BEGIN
  CREATECLIENT('Rúni', 'Klein', 'Hansen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Rúni';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Torkil', 'Janusarson', 'Thomsen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Torkil';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Jákup', 'Pæturssonur', 'Dam', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Jákup';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
  
  CREATECLIENT('Heidi', 'Jónsveinsdóttir', 'Simonsen', 100, '01-JAN-1970', '010170-001');
  SELECT clientid INTO tempclientid FROM client WHERE firstname = 'Heidi';
  CREATEACCOUNT(10000, tempclientid, 14, 'Kredit konto');
  CREATEACCOUNT(0, tempclientid, 15, 'Debit konto');
  CREATEEMPLOYEE('Banker', 100000, tempclientid);
END CREATECLIENTS;
/

create or replace PROCEDURE BOOKKEEPING (
  startdate DATE,
  enddate DATE
)
AS
CURSOR c_transactions is
  select transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount, cid FROM transaction;
b_transactionid NUMBER;
b_transactionpair NUMBER;
b_transactiontype NUMBER;
b_account_number NUMBER;
b_transactiondate DATE;
b_valuedate DATE;
b_message VARCHAR2(255);
b_amount NUMBER;
b_cid NUMBER;
BEGIN
  SAVEPOINT before_booking; 
  DBMS_OUTPUT.put_line('Beginning to book transactions');
  OPEN c_transactions;
  LOOP
    FETCH c_transactions INTO b_transactionid, b_transactionpair, b_transactiontype, b_account_number, b_transactiondate, b_valuedate, b_message, b_amount, b_cid;
    EXIT WHEN c_transactions%notfound;
    IF startdate <= b_transactiondate and b_transactiondate < enddate THEN
      DBMS_OUTPUT.put_line(b_transactionid);
      INSERT INTO bookedtransaction -- (transactionid, transactionpair, transactiontype, account_number, transactiondate, valuedate, message, amount)
      VALUES (b_transactionid, b_transactionpair, b_transactiontype, b_account_number, b_transactiondate, systimestamp, b_message, b_amount, b_cid);
      DELETE FROM transaction WHERE transactionid = transactionid;
    END IF;
  END LOOP;
  EXCEPTION
  --DBMS_OUTPUT.put_line('Exception occured rolling back. [Bookkeeping]');
  WHEN OTHERS THEN
  ROLLBACK TO before_booking;
  COMMIT;
END BOOKKEEPING;
/

CREATE OR REPLACE PROCEDURE BOOKKEEPING2
AS
current_time TIMESTAMP;
last_time TIMESTAMP;
BEGIN
  SELECT systimestamp INTO current_time FROM dual;
  SELECT max(last_booking) INTO last_time FROM bookingtransactions_data;
  bookkeeping(last_time, current_time);
  INSERT INTO BOOKINGTRANSACTIONS_DATA VALUES (BOOK_TRANSACTIONS_SEQUENCE.nextval, current_time);
END BOOKKEEPING2;
/

create or replace procedure CALCULATERATE AS 
current_time DATE;
BEGIN
    SELECT sysdate INTO current_time FROM dual;
    insert into ratecalculation VALUES (null, current_time, 99, 99);
END CALCULATERATE;
/

create or replace PROCEDURE CREATEEMPLOYEE
(
  ROLE IN VARCHAR2 
, SALARY IN NUMBER 
, CLIENTID IN NUMBER
) AS 
BEGIN
  INSERT INTO employee VALUES (ROLE, SALARY, EMPLOYEE_ID_SEQ.nextval, CLIENTID);
END CREATEEMPLOYEE;
/

create or replace procedure CALCULATERATE AS 
current_time DATE;
BEGIN
    SELECT sysdate INTO current_time FROM dual;
    insert into ratecalculation VALUES (null, current_time, 99, 15);
END CALCULATERATE;
/