addresses = []

with open("postnr.txt", 'r', encoding='utf-8') as f:
    for line in f.readlines():
        address = line.split("\t")
        addresses.append((address[0].strip(), address[1].strip()))

print(addresses)

with open("postnummur.txt", 'w', encoding='utf-8') as f:
    for address in addresses:
        zip = address[1]
        bygd = address[0]
        tmp = "insert into Zip values (" + zip + ", '" + bygd + "');\n"
        f.write(tmp)