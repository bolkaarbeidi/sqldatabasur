from flask import Flask, request, render_template, redirect, url_for, session
import cx_Oracle
import os

app = Flask(__name__)
app.secret_key = os.urandom(24).hex()

USERNAME = "dbbook"
PASSWORD = "password"
DSN="localhost/xe"
ORACLE_CLIENT_PATH = r"C:\instantclient_19_10"
#SECRET_KEY = 

# Must be installed and the location put here:
cx_Oracle.init_oracle_client(lib_dir=ORACLE_CLIENT_PATH)
#cx_Oracle.init_oracle_client(lib_dir=r"C:\instantclient_19_10")

def sqlConnection():
    connection = cx_Oracle.connect(
    user=USERNAME,
    password=PASSWORD,
    dsn=DSN)
    return connection

connection = sqlConnection()
cursor = connection.cursor()

def getRows(column, table, where='', wherevalue=''):
    rows = []
    if (where == '' or wherevalue == ''):
        cursor.execute("select '" + column + "' from '" + table + "'")
    else:
        cursor.execute("select '" + column + "' from '" + table + "' WHERE " + where + "='" + wherevalue + "'")
    for row in cursor:
        rows.append(row[0])
    
    return rows

def getClients():
    clients = []
    cursor.execute("select * from client")
    for (fn, mn, ln, cid, zip) in cursor:
        clients.append((fn, mn, ln, cid, zip))

    return clients

def getClientIds():
    clientIds = []
    cursor.execute("SELECT clientid FROM client")
    for clientid in cursor:
        clientIds.append(clientid[0])

    return clientIds

def getClient(clientid):
    clients = []
    cursor.execute("SELECT * FROM client WHERE clientid = " + str(clientid))
    for client in cursor:
        clients.append(client)

    print("length of clients: " + str(len(clients)))
    if (len(clients) == 0):
        return None
    
    return clients[0]

def isEmployee(clientid):
    sql = "SELECT count(*) FROM EMPLOYEE WHERE clientid = " + str(clientid)
    cursor.execute(sql)
    employees = []
    for employee in cursor:
        employees.append(employee)
    return (employees[0][0] >= 1)


def getAccountnumbers(clientid):
    sql = "SELECT account_number FROM account WHERE clientid = '" + str(clientid) + "'"
    accounts = []
    cursor.execute(sql)
    for account in cursor:
        accounts.append(account[0])
    
    return accounts

def getAccounts(clientid):
    sql =   "SELECT * "\
            "FROM account "\
            "WHERE account.CLIENTID = " + str(clientid)
    accounts = []
    cursor.execute(sql)
    for account in cursor:
        accounts.append(account)
    print(accounts)
    return accounts

def getAccountSummaries(clientid):
    sql =   "SELECT * FROM account_summary WHERE clientid = " + str(clientid)
    cursor.execute(sql)
    accounts_summaries = []
    for acc_summ in cursor:
        accounts_summaries.append(acc_summ)
    
    return accounts_summaries

def getTransactions(account_number):
    print("account number: " + str(account_number))
    sql = "SELECT * FROM transaction_summary2 WHERE account_number = " + str(account_number)
    transactions = []
    cursor.execute(sql)
    for transaction in cursor:
        transactions.append(transaction)
    print("transactions:")
    print(transactions)
    return transactions

def createTransaction(accountfrom, accountto, amount, message):
    print("accountfrom: " + str(accountfrom))
    print("accountto: " + str(accountto))
    sql = "call createTransaction(" + str(accountfrom) + ", " + str(accountto) + ", '" + str(message) + "', " + str(amount) + ", " + session['clientid'] + ")"
    cursor.execute(sql)
    cursor.execute("commit")

def createClient(firstname, middlename, lastname, zipcode, birthday, ptal):
    sql = "call createclient('" + firstname + "', '" + middlename + "', '" + lastname + "', '" + str(zipcode) + "', '" + str(birthday) + "', " + str(ptal) + ")"
    cursor.execute(sql)
    cursor.execute("commit")

def getAccountInfoAll(account_number):
    sql = "SELECT * FROM account WHERE account_number = " + str(account_number)
    cursor.execute(sql)
    accountInfo = cursor.fetchone()
    return accountInfo

def createAccountProper(accounttype, overdraw, clientid2, name):
    sql = "call CREATEACCOUNT(" + str(overdraw) + ", " + str(clientid2) + ", " + str(accounttype) + ", '" + str(name) + "')"
    cursor.execute(sql)
    cursor.execute("commit")

def create_deposit(account, amount, message, clientid):
    sql = "call DEPOSIT(" + str(amount) + ", " + str(account) + ", '" + message + "'," + str(clientid) + ")"
    cursor.execute(sql)
    cursor.execute("commit")

def create_withdraw(account, amount, message, clientid):
    sql = "call WITHDRAW(" + str(amount) + ", " + str(account) + ", '" + message + "'," + str(clientid) + ")"
    cursor.execute(sql)
    cursor.execute("commit")

def partner_accounts(clientid):
    sql = "SELECT * FROM partner_accounts3 WHERE clientid = " + str(clientid)
    accounts = []
    cursor.execute(sql)
    for account in cursor:
       accounts.append(account)
    return accounts

def children_accounts(clientid):
    sql = "SELECT * FROM child_accounts3 WHERE clientid = " + str(clientid)
    accounts = []
    cursor.execute(sql)
    for account in cursor:
       accounts.append(account)
    return accounts

@app.route('/logout')
def clearSession():
    session.clear()
    return redirect("index")

@app.route('/index')
@app.route('/')
def index():
    clearSession()
    return render_template("index.html", message="")

@app.route('/clients')
def clients():
    clients = getClients()
    return render_template("clients.html", clients=clients)

def renderName(client):
    firstName   = client[0]
    middleName  = client[1]
    lastName    = client[2]
    name        = ""

    if (firstName != None):
        name += firstName
    
    if (middleName != None):
        name += " " + middleName
    
    if (lastName != None):
        name += " " + lastName
    
    return name

@app.route('/accounts', methods=['POST', 'GET'])
def accounts():
    if 'clientid' not in session:
        if 'clientid' not in request.form:
            return redirect(url_for('index'))
        session['clientid'] = request.form['clientid']

    accounts = getAccounts(session['clientid'])
    children_accounts_list = children_accounts(session['clientid'])
    partner_accounts_list = partner_accounts(session['clientid'])
    if isEmployee(session['clientid']):
        print("is employee")
        employee = 1
    else:
        print("is not employee")
        employee = 0

    client = getClient(session['clientid'])
    print(client)
    if client == None:
        return redirect('index')

    clientName = renderName(client)
    account_types = {14: "Credit", 15: "Debit", 16: "Other", 88: "Equity", 89: "Balance"}
    return render_template("accounts.html", accounts=accounts, cname=clientName, account_number=session['clientid'], employee=employee, cal=children_accounts_list, pal=partner_accounts_list, at=account_types)

@app.route('/lg')
def lg():
    session['clientid']

@app.route('/loggedin', methods=['POST', 'GET'])
def loggedin():
    print("loggedin function here!")
    if request.method == 'POST':
        return render_template("testsite.html", method=request.method)
    
    if request.method == 'GET':
        return render_template("testsite.html", method=request.method)

@app.route('/account', methods=['POST'])
def account():
    account_number = request.form['account_number']
    transactions = getTransactions(account_number)
    account_info = getAccountInfoAll(account_number)
    cname = renderName(getClient(session['clientid']))
    return render_template("account.html", transactions=transactions, cname=cname, account_number=account_number, account_info=account_info)
    #return render_template("account.html", transactions=transactions, unbooked_transactions=unbooked_transactions, cname=cname, account_number=account_number, account_info=account_info)

@app.route('/transaction')
def transaction():
    accounts = getAccountnumbers(session['clientid'])
    return render_template("transaction.html", accounts=accounts)

@app.route('/trverify', methods=['POST'])
def verifyTransaction():
    accountfrom = request.form['accountfrom']
    accountto = request.form['accountto']
    amount = request.form['amount']
    message = request.form['message']
    createTransaction(accountfrom, accountto, amount, message)
    return redirect(url_for('accounts'))

@app.route('/createaccount')
def createAccount():
    clientIds = getClientIds()
    return render_template('createaccount.html', clientIds=clientIds)

@app.route('/createaccount2', methods=['POST'])
def createAccount3():
    accounttype = request.form['accounttype']
    overdraw = request.form['overdraw']
    clientid2 = request.form['clientid2']
    name = request.form['accountname']
    createAccountProper(accounttype, overdraw, clientid2, name)
    return redirect(url_for('accounts'))

@app.route('/createclient')
def createClient2():
    return render_template('createclient.html')

@app.route('/createclient2', methods=['POST'])
def createClient3():
    firstname = request.form['firstname']
    middlename = request.form['middlename']
    lastname = request.form['lastname']
    zipcode = int(request.form['zipcode'])
    bod = request.form['bod']
    ptal = request.form['ptal']
    createClient(firstname, middlename, lastname, zipcode, bod, ptal)
    return redirect(url_for('employeeIndex'))
    #return redirect(url_for('accounts'))



@app.route('/employee', methods=['POST', 'GET'])
def employeeIndex():
    clients = getClientIds()
    allInfoClients = dict()
    for client in clients:
        name = renderName(getClient(client))
        allInfoClients[name] = getAccountSummaries(client)
    return render_template('employee.html', allClients=allInfoClients)

@app.route('/deposit')
def deposit():
    return render_template('deposit.html')

@app.route('/deposit-verify', methods=['POST'])
def deposit_verify():
    account = request.form['account']
    amount = request.form['amount']
    message = request.form['message']
    create_deposit(account, amount, message, session['clientid'])
    return redirect(url_for('accounts'))

@app.route('/withdraw')
def withdraw():
    return render_template('withdraw.html')

@app.route('/withdraw-verify', methods=['POST'])
def withdraw_verify():
    account = request.form['account']
    amount = request.form['amount']
    message = request.form['message']
    create_withdraw(account, amount, message, session['clientid'])
    return redirect(url_for('accounts'))


@app.route('/about')
def aboutpage():
    return render_template('about.html')